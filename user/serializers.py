from django.contrib.auth import get_user_model
from rest_framework import serializers

from company.models import Company, Membership


class UserCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name']


class MembershipSerializer(serializers.ModelSerializer):
    company = UserCompanySerializer(many=False, read_only=True)

    class Meta:
        model = Membership
        fields = ['id', 'company']


class UserSerializer(serializers.ModelSerializer):
    owned_companies = UserCompanySerializer(many=True, read_only=True)
    member_in_companies = MembershipSerializer(many=True, read_only=True, source="companies")

    class Meta:
        model = get_user_model()
        fields = ["id", "email", "first_name", "last_name",
                  "is_staff", "is_active", "is_superuser",
                  "owned_companies", "member_in_companies"]
