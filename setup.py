#!/usr/bin/env python
from setuptools import setup, find_packages


setup(
    name='coursemanager2',
    version="0.3.1",
    author="Marco Schlicht",
    author_email="m.schlicht@hackademy.cloud",
    description="Api for Hackademy",
    packages=find_packages(),
    scripts=['manage.py'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8.2',
)
