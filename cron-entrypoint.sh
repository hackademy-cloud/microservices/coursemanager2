#!/bin/bash

# save environment variables in .env file, so that they can be accessed by the cronjobs
printenv > /opt/app/coursemanager2/.env

# add the cronjobs
python manage.py crontab add

# start cron daemon
service cron start

# show the log of the cronjobs (they need to write into this file)
tail -f /var/log/cron.log
