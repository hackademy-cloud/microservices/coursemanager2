from collections import OrderedDict
from distutils import util
from typing import Dict, Type, Callable, Any

from django.forms import fields, forms
from django.template import loader
from rest_framework.exceptions import ValidationError

from django_filters.utils import get_model_field
from django.db.models import BooleanField
from django.db.models.fields import Field

from commons.utils import ModelCollection


class DontFilterException(Exception):
    """This can be thrown in Filters to skip the filter process."""


class CollectionFilter:
    """Filters items from a collection that are equals to the query parameter with the name of the field."""

    def __init__(self, field_name, cast_method, form, error_message):
        self.field_name = field_name
        self.cast_method = cast_method
        self.form = form
        self.error_message = error_message

    def filter(self, request, queryset):
        value_raw = request.query_params.get(self.field_name, None)
        if value_raw is None or value_raw == "":
            return queryset
        try:
            value = self.cast_method(value_raw)
        except ValueError:
            raise ValidationError(self.error_message)
        except DontFilterException:
            return queryset
        return filter(lambda obj: getattr(obj, self.field_name) == value, queryset)


filter_factory = Callable[[Any, str], CollectionFilter]


def boolean_filter(request, attribute) -> CollectionFilter:
    """This creates a ManualQueryFilter"""
    def strtobool(val):
        """used to convert a string to a boolean."""
        if val == "unknown":
            # raised to skip filtering
            raise DontFilterException()
        return bool(util.strtobool(val))
    # raw value used in the form as default value
    value_raw = request.query_params.get(attribute, None)
    form = fields.NullBooleanField(required=False, initial=value_raw, label=attribute)
    return CollectionFilter(attribute, cast_method=strtobool, form=form, error_message="Enter a boolean.")


"""Default mappings from model fields to filter factory."""
FIELD_DEFAULTS: Dict[Type[Field], filter_factory] = {
    BooleanField: boolean_filter,
}


class CollectionFilterSet:
    """Automatically creates collection filters to the filterset_fields variable of the given view."""
    FIELD_DEFAULTS = FIELD_DEFAULTS

    def __init__(self, request, queryset, view):
        self.request = request
        self.queryset = queryset
        self.view = view

    @property
    def form(self):
        filters = self.get_filters()
        fieldset = OrderedDict([
            (filter.field_name, filter.form)
            for filter in filters])
        return type(str('%sForm' % self.__class__.__name__),
                    (forms.Form, ), fieldset)

    def get_filters(self):
        model = self.queryset.model
        filterset_fields = getattr(self.view, 'filterset_fields', [])
        filterset = []
        for attribute in filterset_fields:
            field = get_model_field(model, attribute)
            method = self.FIELD_DEFAULTS.get(field.__class__, None)
            if method is not None:
                filterset.append(method(self.request, attribute))
            else:
                raise NotImplementedError("Currently not implemented.")
        return filterset


class CollectionFilterBackend:
    """Filter backend that can be used with collections instead of querysets."""

    filterset_base = CollectionFilterSet

    def get_filterset(self, request, queryset, view):
        return self.filterset_base(request, queryset, view)

    def filter(self, request, queryset, view):
        model = queryset.model
        filterset = self.get_filterset(request, queryset, view)
        errors = {}
        for filt in filterset.get_filters():
            try:
                queryset = filt.filter(request, queryset)
            except ValidationError as e:
                errors[filt.field_name] = e.detail
        if len(errors.items()) > 0:
            raise ValidationError(errors)
        return ModelCollection(model, queryset)

    def to_html(self, request, queryset, view):
        """Adds the formular buttons to the filter formular in the automatically generated documentation."""
        filterset = self.get_filterset(request, queryset, view)
        if filterset is None:
            return None
        template = loader.get_template(self.template)
        context = {'filter': filterset}
        return template.render(context, request)

    @property
    def template(self):
        return 'django_filters/rest_framework/form.html'
