from copy import deepcopy

import django_filters
from django.db import models
from django_filters.rest_framework import DjangoFilterBackend
from django_filters.rest_framework.filterset import FILTER_FOR_DBFIELD_DEFAULTS

PRIVACY_FILTER_DEFAULTS = deepcopy(FILTER_FOR_DBFIELD_DEFAULTS)
PRIVACY_FILTER_DEFAULTS.update({
    models.ForeignKey: {
        'filter_class': django_filters.NumberFilter,
    }
})


class PrivacyFilterSet(django_filters.FilterSet):
    FILTER_DEFAULTS = PRIVACY_FILTER_DEFAULTS


class PrivacyFilterBackend(DjangoFilterBackend):
    """Replaces the default filterset with the FixedFilter."""
    filterset_base = PrivacyFilterSet
