from lookupy import Collection


class ModelCollection(Collection):
    def __init__(self, _model, _data):
        super().__init__(_data)
        self._model = _model

    @property
    def model(self):
        return self._model


class LazyModelCollection():
    def __init__(self, _model, _data_factory, *args, **kwargs):
        self.model = _model
        self.data_factory = _data_factory
        self.data = None
        self.args = args
        self.kwargs = kwargs

    def __iter__(self):
        if self.data is None:
            self.data = self.data_factory(*self.args, **self.kwargs)
        return self.data.__iter__()

    def __getattr__(self, function):
        """Only invoked if attribute is not found the normal way"""
        if self.data is None:
            self.data = self.data_factory(*self.args, **self.kwargs)
        return getattr(self.data, function)
