from typing import TypeVar, Generic, Dict, Any, Type, List, Tuple

from commons.models import DeleteModel

ExternalType = TypeVar('ExternalType')
ModelType = TypeVar('ModelType')


class ExternalAPIAbstractFactory(Generic[ExternalType]):
    def get_data(self, old_obj: ExternalType) -> Dict[str, Any]:
        raise NotImplementedError()

    def get_filter_kriteria(self, old_obj: ExternalType) -> Dict[str, Any]:
        raise NotImplementedError()

    def __init__(self, model: Type[DeleteModel]):
        self.model = model

    def multi_update(self, old_objs: List[ExternalType]):
        pks_new = []
        for old_obj in old_objs:
            data = self.get_data(old_obj)
            filter_criteria = self.get_filter_kriteria(old_obj)
            obj, created = self.model.objects.update_or_create(**filter_criteria, defaults=data)
            pks_new.append(obj.pk)
        delete_list = self.model.objects.exclude(pk__in=pks_new)
        for delete_item in delete_list:
            delete_item.deleted = True
            delete_item.save()
