from __future__ import annotations

from typing import Callable, Any


class Container:
    def __init__(self):
        self.instance_container = {}
        self.factory_container = {}
        self.class_container = {}

    def keys(self):
        key_list = list(self.instance_container.keys())
        key_list.extend(self.factory_container.keys())
        key_list.extend(self.class_container.keys())
        return key_list

    def get(self, key_type):
        if key_type in self.instance_container:
            return self.instance_container[key_type]
        elif key_type in self.factory_container:
            return self.factory_container[key_type](self)
        elif key_type in self.class_container:
            method = self.class_container[key_type]
            return self.class_inject(method)
        raise KeyError(key_type)

    def class_inject(self, cls):
        kwargs = {}
        for key, key_type in cls.__init__.__annotations__.items():
            if key_type in self.keys():
                val = self.get(key_type)
                kwargs[key] = val
        return cls(**kwargs)

    def func_inject(self, func):
        def injection(*args, **kwargs):
            for key, key_type in func.__annotations__.items():
                if key in kwargs:
                    continue
                if key_type in self.keys():
                    val = self.get(key_type)
                    kwargs[key] = val
            return func(*args, **kwargs)
        return injection

    def register_instance(self, key_type, instance):
        """Everytime this key is needed, the instance is injected."""
        self.instance_container[key_type] = instance

    def register_factory(self, key_type, _callable: Callable[[Container], Any]):
        """Everytime this key is needed, the result callable is executed and the result is injected."""
        self.factory_container[key_type] = _callable

    def register_singleton(self, key_type, cls=None):
        """Everytime this key is needed, the same instance of the class is injected."""
        if cls is None:
            cls = key_type
        instance = self.class_inject(cls)
        self.instance_container[key_type] = instance

    def register_transient(self, key_type, cls=None):
        """Everytime this key is needed, a new instance of the class is created and injected."""
        if cls is None:
            cls = key_type
        self.class_container[key_type] = cls


default_container = Container()


def inject(func):
    return default_container.func_inject(func)