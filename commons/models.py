from django.db import models


class DeleteModel(models.Model):
    deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True
