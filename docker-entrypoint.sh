#!/bin/bash

# Collect static files
echo "Collect static files"
python manage.py collectstatic --noinput

# Apply database migrations
echo "Apply database migrations"
python manage.py migrate

# Create superuser
echo "Creating superuser"
if [ -n "$DJANGO_SUPERUSER_EMAIL" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] ; then
    python manage.py createsuperuser --no-input
fi

# Start server
echo "Starting server"
#chown -R www-data:www-data /opt/app/coursemanager2/volumes # otherwise the database is not accessible from the workers; only needed when we add volumes
(gunicorn coursemanager2.wsgi --user www-data --bind 0.0.0.0:8010 --workers "$AMOUNT_WORKERS") &
nginx -g "daemon off;" # needed for static files