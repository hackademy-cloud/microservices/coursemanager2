from ai_django_core.admin import ReadOnlyAdmin
from django.contrib import admin
from lods.models import Catalog, Series, Course, DeliveryRegions, LabHistory


class CatalogAdmin(ReadOnlyAdmin):
    pass


class SeriesAdmin(ReadOnlyAdmin):
    pass


class CourseAdmin(ReadOnlyAdmin):
    pass


class DeliveryRegionsAdmin(ReadOnlyAdmin):
    pass


class LabHistoryAdmin(ReadOnlyAdmin):
    class Meta:
        ordering = ('-date',)


admin.site.register(Catalog, CatalogAdmin)
admin.site.register(Series, SeriesAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(DeliveryRegions, DeliveryRegionsAdmin)
admin.site.register(LabHistory, LabHistoryAdmin)
