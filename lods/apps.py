from django.apps import AppConfig


class LodsConfig(AppConfig):
    name = 'lods'

    def ready(self):
        from lods_lib.client import ILodsApiClient, LodsApiClient
        from commons.dependency_injection import default_container
        from coursemanager2 import settings
        from lods.factories.lab_factory import ILabsFactory, LabsFactory
        from lods.services.launch_course_service import ILaunchCourseService, LaunchCourseService
        from lods.services.destroy_lab_service import IDestroyLabService, DestroyLabService
        from lods.services.resume_lab_service import IResumeLabService, ResumeLabService
        from lods.services.save_lab_service import SaveLabService, ISaveLabService
        from lods.factories.catalog_factory import CatalogFactory
        default_container.register_instance(ILodsApiClient, LodsApiClient(settings.LODS_API_KEY, settings.LODS_API_URL))
        default_container.register_transient(ILabsFactory, LabsFactory)
        default_container.register_transient(ILaunchCourseService, LaunchCourseService)
        default_container.register_transient(IDestroyLabService, DestroyLabService)
        default_container.register_transient(IResumeLabService, ResumeLabService)
        default_container.register_transient(ISaveLabService, SaveLabService)
        default_container.register_transient(CatalogFactory)
