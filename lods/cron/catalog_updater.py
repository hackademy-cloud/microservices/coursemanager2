from commons.dependency_injection import inject
from lods.factories.catalog_factory import CatalogFactory

import logging

logger = logging.getLogger(__name__)


@inject
def update_catalog_job(catalog_factory: CatalogFactory):
    logger.info("Updating catalog.")
    catalog = catalog_factory.populate_data()
    if catalog.status == 1:
        logger.info("Catalog updated successfully.")
    else:
        logger.warning(f"Catalog update failed: {catalog.error} ({catalog.status}")
