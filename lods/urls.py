from rest_framework.routers import DefaultRouter

from .views import SeriesViewSet, CourseViewSet, LabViewSet, LabHistoryViewSet

app_name = 'lods'

router = DefaultRouter()
router.register('series', SeriesViewSet, basename='series')
router.register('course', CourseViewSet, basename='course')
router.register('lab', LabViewSet, basename='lab')
router.register('lab_history', LabHistoryViewSet, basename='lab_history')

urlpatterns = router.urls
