from django.contrib.auth import get_user_model
from rest_framework import serializers

from lods.models import Course, Series, Lab, LabHistory


class SeriesCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        # deleted is excluded, because deleted courses are not shown, so it will always be true
        # and catalog is excluded, because it's irrelevant
        exclude = ["catalog", "deleted"]
        # we have our own price model and don't use lods prices
        exclude += ["basic_price", "premium_price"]
        # number, cloud_platform_id and ram is irrelevant
        exclude += ["number", "cloud_platform_id", "ram"]
        # we don't use exams, classes and integrated contents
        exclude += ["is_exam", "shared_class_environment_role_id", "has_integrated_content"]
        # platform_id, development_status_id and content_version is only used in the detailed view, because its
        # irrelevant here
        exclude += ["platform_id", "development_status_id", "content_version"]
        # LabSeriesLabProfileSerializer is only used in LabSeriesSerializer, so the series field is redundant
        exclude += ["series"]
        # we don't know how to set scenario and objective, so it's excluded too
        exclude += ["scenario", "objective"]


class SeriesSerializer(serializers.ModelSerializer):
    courses = SeriesCourseSerializer(many=True, read_only=True)

    class Meta:
        model = Series
        fields = ["id", "name", "description", "num_training_days", "courses"]


class SeriesExtendedSerializer(SeriesSerializer):
    is_payed = serializers.SerializerMethodField()

    def get_is_payed(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return obj.is_payed(user)
        return None

    class Meta:
        model = Series
        fields = ["id", "name", "description", "num_training_days", "courses", "is_payed"]


class CourseSeriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Series
        fields = ["id", "name", "description", "num_training_days"]


class CourseSerializer(serializers.ModelSerializer):
    series = CourseSeriesSerializer(many=False, read_only=True)

    class Meta:
        model = Course
        # deleted is excluded, because deleted courses are not shown, so it will always be true
        # and catalog is excluded, because it's irrelevant
        exclude = ["catalog", "deleted"]
        # we have our own price model and don't use lods prices
        exclude += ["basic_price", "premium_price"]
        # number, cloud_platform_id and ram is irrelevant
        exclude += ["cloud_platform_id", "ram"]
        # we don't use exams, classes and integrated contents
        exclude += ["is_exam", "shared_class_environment_role_id", "has_integrated_content"]
        # we don't know how to set scenario and objective, so it's excluded too
        exclude += ["scenario", "objective"]


class CourseExtendedSerializer(CourseSerializer):
    is_payed = serializers.SerializerMethodField()

    def get_is_payed(self, obj):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return obj.is_payed(user)
        return None


class LabCourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'name', 'number']


class LabUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'first_name', 'last_name']


class LabSerializer(serializers.ModelSerializer):
    course = LabCourseSerializer(many=False, read_only=True)
    user = LabUserSerializer(many=False, read_only=True)

    class Meta:
        model = Lab
        fields = '__all__'


class LaunchSerializer(serializers.Serializer):
    result = serializers.IntegerField(source='Result', required=False, allow_null=True, read_only=True)
    url = serializers.CharField(source='Url', required=False, allow_null=True, read_only=True)
    lab_instance_id = serializers.IntegerField(source='LabInstanceId', required=False, allow_null=True, read_only=True)
    expires = serializers.IntegerField(source='Expires', required=False, allow_null=True, read_only=True)
    error = serializers.CharField(source='Error', required=False, allow_null=True, read_only=True)
    status = serializers.IntegerField(source='Status', required=False, allow_null=True, read_only=True)


class LabHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = LabHistory
        fields = '__all__'