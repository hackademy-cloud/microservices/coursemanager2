from lods_lib.api_methods.cancel import CancelParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError

import logging

from lods.models import Lab, LabHistory

logger = logging.getLogger(__name__)


class IDestroyLabService:
    def execute(self, instance):
        raise NotImplementedError()


class DestroyLabService(IDestroyLabService):
    def __init__(self, lods_api_client: ILodsApiClient):
        self.lods_api_client = lods_api_client

    def execute(self, instance: Lab):
        params = CancelParameters(labInstanceId=instance.id)
        cancel = self.lods_api_client.cancel(params)
        LabHistory.objects.create(user=instance.user, course=instance.course, action="cancel", status=cancel.Status, error_message=cancel.Error)
        if cancel.Status == 1:
            return cancel
        else:
            logger.error(
                "Canceling a lab wasn't successful.",
                extra={'input': params.__dict__, 'output': cancel.__dict__}
            )
            raise InternalError(detail="An internal error occurred while canceling the lab.")
