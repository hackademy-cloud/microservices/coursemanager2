import logging

from lods_lib.api_methods.save import SaveResponse, SaveParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import LabHistory

logger = logging.getLogger(__name__)


class ISaveLabService:
    def execute(self, instance):
        raise NotImplementedError()


class SaveLabService(ISaveLabService):
    def __init__(self, lods_api_client: ILodsApiClient):
        self.lods_api_client = lods_api_client

    def execute(self, instance):
        params = SaveParameters(labInstanceId=instance.id)
        saved: SaveResponse = self.lods_api_client.save(params)
        LabHistory.objects.create(user=instance.user, course=instance.course, action="save", status=saved.Status, error_message=saved.Error)
        out = saved.__dict__
        out['LabInstanceId'] = instance.id
        if saved.Status == 1:
            return out
        elif saved.Status in [2]:
            logger.warning(
                "Saving a lab wasn't successful.",
                extra={'input': params.__dict__, 'output': saved.__dict__}
            )
            errors = {
                2: "User has too many active labs",
            }
            raise Conflict(
                errors.get(
                    saved.Status,
                    "An error occurred while saving the lab."
                ))
        else:
            logger.error(
                "Saving a lab wasn't successful.",
                extra={'input': params.__dict__, 'output': saved.__dict__}
            )
            raise InternalError("An internal error occurred while saving the lab, please contact an administrator.")
