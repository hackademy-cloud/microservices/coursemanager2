import logging

from lods_lib.api_methods.resume import ResumeParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import LabHistory

logger = logging.getLogger(__name__)


class IResumeLabService:
    def execute(self, instance):
        raise NotImplementedError()


class ResumeLabService(IResumeLabService):
    def __init__(self, lods_api_client: ILodsApiClient):
        self.lods_api_client = lods_api_client

    def execute(self, instance):
        params = ResumeParameters(labInstanceId=instance.id)
        resume = self.lods_api_client.resume(params)
        LabHistory.objects.create(user=instance.user, course=instance.course, action="resume", status=resume.Status, error_message=resume.Error)
        out = resume.__dict__
        out['LabInstanceId'] = instance.id
        if resume.Status == 1:
            return out
        elif resume.Status in [2]:
            logger.warning(
                "Resume a lab wasn't successful.",
                extra={'input': params.__dict__, 'output': resume.__dict__}
            )
            errors = {
                2: "User has too many active labs",
            }
            raise Conflict(
                errors.get(
                    resume.Status,
                    "An error occurred while resuming the lab, please contact an administrator."
                ))
        else:
            logger.error(
                "Resume a lab wasn't successful.",
                extra={'input': params.__dict__, 'output': resume.__dict__}
            )
            raise InternalError("An internal error occurred while resuming the lab, please contact an administrator.")
