import logging

from lods_lib.api_methods.launch import LaunchParameters, LaunchResponse
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import Course, LabHistory

logger = logging.getLogger(__name__)


class ILaunchCourseService:
    def execute(self, course: Course, user) -> LaunchResponse:
        raise NotImplementedError()


class LaunchCourseService(ILaunchCourseService):
    def __init__(self, lods_api_client: ILodsApiClient):
        self.lods_api_client = lods_api_client

    def execute(self, course: Course, user):
        # first and last name are not allowed to be empty, so a default must be set
        params = LaunchParameters(
            labId=course.id,
            userId=user.id,
            firstName=user.first_name or "Anonymous",
            lastName=user.last_name or "Person",
            email=user.email,
        )
        logger.info(f"Launching course.", extra={'input': params.__dict__})
        response = self.lods_api_client.launch(params)
        LabHistory.objects.create(user=user, course=course, action="launch", status=response.Status, error_message=response.Error)
        if response.Status == 1:
            # no errors
            return response
        elif response.Status in [2, 6, 20, 30, 100]:
            # errors showing to the user
            logger.warning(
                "Error while launching a lab.",
                extra={
                    'input': params.__dict__,
                    'output': response.__dict__,
                    'documentation_url': "https://github.com/LearnOnDemandSystems/docs/blob/master/lod/lod-api/lod-api-launch.md#response"
                })
            errors = {
                2: "User has too many active labs",
                6: "User has a saved instance of this lab",
                20: "User's organization has too many active labs",
                30: "User's organization doesn't have enough available RAM",
                100: "User has launched the maximum number of instances of this lab profile",
            }
            raise Conflict(
                errors.get(
                    response.Status,
                    "An error occurred while launching the lab, please contact an administrator."
                ))
        else:
            # errors to show to us
            logger.error(
                "Error while launching a lab.",
                extra={
                    'input': params.__dict__,
                    'output': response.__dict__,
                    'documentation_url': "https://github.com/LearnOnDemandSystems/docs/blob/master/lod/lod-api/lod-api-launch.md#response"
                })
            raise InternalError("An internal error occurred while launching the lab, please contact an administrator.")
