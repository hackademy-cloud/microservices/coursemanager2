from ddt import ddt, data
from django.contrib.auth import get_user_model
from django.test import testcases
from lods_lib.api_methods.resume import ResumeResponse, ResumeParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import Catalog, Series, Course, Lab, LabHistory
from lods.services.resume_lab_service import ResumeLabService


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, resume_ret: ResumeResponse):
        self.resume_ret = resume_ret

    def resume(self, params: ResumeParameters) -> ResumeResponse:
        return self.resume_ret


@ddt
class ResumeLabServiceTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                            catalog=self.catalog)
        self.user = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.lab = Lab(course=self.course, user=self.user)

    def test_multi_update(self):
        resume_output = ResumeResponse(Status=1, Error="", Result=0, Url="", Expires=0)
        self.lods_api_client = LodsApiClientMockup(resume_output)
        self.resume_lab_service = ResumeLabService(self.lods_api_client)
        response = self.resume_lab_service.execute(self.lab)
        # test if has worked
        self.assertEqual(response['Status'], 1)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "resume")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, resume_output.Status)
        self.assertEqual(last_history.error_message, resume_output.Error)

    def test_multi_update_failed_warnings(self):
        resume_output = ResumeResponse(Status=2, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(resume_output)
        self.resume_lab_service = ResumeLabService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(Conflict):
            self.resume_lab_service.execute(self.lab)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "resume")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, resume_output.Status)
        self.assertEqual(last_history.error_message, resume_output.Error)

    @data(0, 10, 40, 50)
    def test_multi_update_failed_errors(self, value):
        resume_output = ResumeResponse(Status=value, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(resume_output)
        self.resume_lab_service = ResumeLabService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(InternalError):
            self.resume_lab_service.execute(self.lab)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "resume")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, resume_output.Status)
        self.assertEqual(last_history.error_message, resume_output.Error)
