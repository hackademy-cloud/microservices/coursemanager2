from django.contrib.auth import get_user_model
from django.test import testcases
from lods_lib.api_methods.cancel import CancelResponse, CancelParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError
from lods.models import Lab, Catalog, Series, Course, LabHistory
from lods.services.destroy_lab_service import DestroyLabService


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, cancel_ret: CancelResponse):
        self.cancel_ret = cancel_ret

    def cancel(self, params: CancelParameters) -> CancelResponse:
        return self.cancel_ret


class DestroyLabServiceTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                             catalog=self.catalog)
        self.user = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.lab = Lab(course=self.course, user=self.user)

    def test_multi_update(self):
        cancel_output = CancelResponse(Status=1, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(cancel_output)
        self.destroy_lab_service = DestroyLabService(self.lods_api_client)
        response = self.destroy_lab_service.execute(self.lab)
        # test if has worked
        self.assertEqual(response.Status, 1)
        self.assertEqual(response, cancel_output)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "cancel")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, cancel_output.Status)
        self.assertEqual(last_history.error_message, cancel_output.Error)

    def test_multi_update_failed(self):
        cancel_output = CancelResponse(Status=0, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(cancel_output)
        self.destroy_lab_service = DestroyLabService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(InternalError):
            self.destroy_lab_service.execute(self.lab)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "cancel")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, cancel_output.Status)
        self.assertEqual(last_history.error_message, cancel_output.Error)
