from ddt import ddt, data
from django.contrib.auth import get_user_model
from django.test import testcases
from lods_lib.api_methods.save import SaveResponse, SaveParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import Catalog, Series, Course, Lab, LabHistory
from lods.services.save_lab_service import SaveLabService


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, save_ret: SaveResponse):
        self.save_ret = save_ret

    def save(self, params: SaveParameters) -> SaveResponse:
        return self.save_ret


@ddt
class SaveLabServiceTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                            catalog=self.catalog)
        self.user = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.lab = Lab(course=self.course, user=self.user)

    def test_multi_update(self):
        save_output = SaveResponse(Status=1, Error="", Result=0, Expires=0)
        self.lods_api_client = LodsApiClientMockup(save_output)
        self.save_lab_service = SaveLabService(self.lods_api_client)
        response = self.save_lab_service.execute(self.lab)
        # test if has worked
        self.assertEqual(response['Status'], 1)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "save")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, save_output.Status)
        self.assertEqual(last_history.error_message, save_output.Error)

    def test_multi_update_failed_warnings(self):
        save_output = SaveResponse(Status=2, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(save_output)
        self.save_lab_service = SaveLabService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(Conflict):
            self.save_lab_service.execute(self.lab)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "save")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, save_output.Status)
        self.assertEqual(last_history.error_message, save_output.Error)

    @data(0, 10, 40, 50)
    def test_multi_update_failed_errors(self, value):
        save_output = SaveResponse(Status=value, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(save_output)
        self.save_lab_service = SaveLabService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(InternalError):
            self.save_lab_service.execute(self.lab)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "save")
        self.assertEqual(last_history.user, self.lab.user)
        self.assertEqual(last_history.course, self.lab.course)
        self.assertEqual(last_history.status, save_output.Status)
        self.assertEqual(last_history.error_message, save_output.Error)
