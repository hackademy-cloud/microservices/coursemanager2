from ddt import ddt, data
from django.contrib.auth import get_user_model
from django.test import testcases
from lods_lib.api_methods.launch import LaunchResponse, LaunchParameters
from lods_lib.client import ILodsApiClient

from lods.exceptions import InternalError, Conflict
from lods.models import Catalog, Series, Course, LabHistory
from lods.services.launch_course_service import LaunchCourseService


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, launch_ret: LaunchResponse):
        self.launch_ret = launch_ret

    def launch(self, params: LaunchParameters) -> LaunchResponse:
        return self.launch_ret


@ddt
class LaunchCourseServiceTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                            catalog=self.catalog)
        self.user = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")

    def test_multi_update(self):
        launch_output = LaunchResponse(Status=1, Error="", Result=0, Url="", LabInstanceId=0, Expires=0)
        self.lods_api_client = LodsApiClientMockup(launch_output)
        self.launch_course_service = LaunchCourseService(self.lods_api_client)
        response = self.launch_course_service.execute(self.course, self.user)
        # test if has worked
        self.assertEqual(response.Status, 1)
        self.assertEqual(response, launch_output)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "launch")
        self.assertEqual(last_history.user, self.user)
        self.assertEqual(last_history.course, self.course)
        self.assertEqual(last_history.status, response.Status)
        self.assertEqual(last_history.error_message, response.Error)

    def test_empty_user_first_last_name(self):
        this = self
        class AssertNamesLodsApiClientMockup(ILodsApiClient):
            def launch(self, params: LaunchParameters) -> LaunchResponse:
                # first and last name are not allowed to be empty, so a default must be set
                this.assertEqual(params.firstName, "Anonymous")
                this.assertEqual(params.lastName, "Person")
                return LaunchResponse(Status=1, Error="", Result=0, Url="", LabInstanceId=0, Expires=0)
        self.lods_api_client = AssertNamesLodsApiClientMockup()
        self.launch_course_service = LaunchCourseService(self.lods_api_client)
        self.launch_course_service.execute(self.course, self.user)

    @data(2, 6, 20, 30, 100)
    def test_multi_update_failed_warnings(self, value):
        launch_output = LaunchResponse(Status=value, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(launch_output)
        self.launch_course_service = LaunchCourseService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(Conflict):
            self.launch_course_service.execute(self.course, self.user)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "launch")
        self.assertEqual(last_history.user, self.user)
        self.assertEqual(last_history.course, self.course)
        self.assertEqual(last_history.status, launch_output.Status)
        self.assertEqual(last_history.error_message, launch_output.Error)

    @data(0, 10, 40, 50)
    def test_multi_update_failed_errors(self, value):
        launch_output = LaunchResponse(Status=value, Error="", Result=0)
        self.lods_api_client = LodsApiClientMockup(launch_output)
        self.launch_course_service = LaunchCourseService(self.lods_api_client)
        # test if has worked
        with self.assertRaises(InternalError):
            self.launch_course_service.execute(self.course, self.user)
        # test if it was logged
        last_history = LabHistory.objects.last()
        self.assertEqual(last_history.action, "launch")
        self.assertEqual(last_history.user, self.user)
        self.assertEqual(last_history.course, self.course)
        self.assertEqual(last_history.status, launch_output.Status)
        self.assertEqual(last_history.error_message, launch_output.Error)
