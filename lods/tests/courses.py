import json

from django.urls import reverse
from lods_lib.api_methods.launch import LaunchResponse
from commons.dependency_injection import default_container
from commons.utils_testing import TokenLoginAPITestCase
from lods.models import Catalog, Course
from lods.services.launch_course_service import ILaunchCourseService
from lods.tests.utils import SeriesAndCourseAPIViewTestCaseBase, LaunchAPIViewTestCaseBase
from lods.views import CourseViewSet


class CourseListAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    url = reverse("lods:series-list")

    def test_get_course_without_admin(self):
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, 200)

    def test_get_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, 200)

    def test_get_course_without_login(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)


class CourseDetailAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):

    def url(self, pk):
        return reverse("lods:course-detail", args=[pk])

    def test_get_course_without_admin(self):
        response = self.client.get(self.url(self.course_1.id))
        self.assertDictEqual(json.loads(response.content), self.course_1_json)
        self.assertEqual(response.status_code, 200)

    def test_get_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url(self.course_1.id))
        self.assertDictEqual(response.data, self.course_1_json)
        self.assertEqual(response.status_code, 200)

    def test_get_course_without_login(self):
        self.client.logout()
        response = self.client.get(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 401)


class CoursePostAPIViewTestCase(TokenLoginAPITestCase):
    url = reverse("lods:course-list")

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_post_course_without_admin(self):
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Course.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(Course.objects.all()), 0)

    def test_post_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Course.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(Course.objects.all()), 0)

    def test_post_course_without_login(self):
        self.client.logout()
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Course.objects.all()), 0)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(len(Course.objects.all()), 0)


class CoursePutAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:course-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_put_course_without_admin(self):
        response = self.client.put(self.url(self.course_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 405)

    def test_put_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.put(self.url(self.course_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 405)

    def test_put_course_without_login(self):
        self.client.logout()
        response = self.client.put(self.url(self.course_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 401)


class CoursePatchAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:course-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_patch_course_without_admin(self):
        response = self.client.patch(self.url(self.course_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 405)

    def test_patch_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.patch(self.url(self.course_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 405)

    def test_patch_course_without_login(self):
        self.client.logout()
        response = self.client.patch(self.url(self.course_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 401)


class CourseDeleteAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:course-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_delete_course_without_admin(self):
        response = self.client.delete(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_course_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.delete(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_course_without_login(self):
        self.client.logout()
        response = self.client.delete(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 401)


class LaunchCourseServiceMockup(ILaunchCourseService):
    def __init__(self, launch_ret: LaunchResponse):
        self.launch_ret = launch_ret

    def execute(self, course: Course, user) -> LaunchResponse:
        return self.launch_ret


def launch_json(launch_response: LaunchResponse):
    return {
        "result": launch_response.Result,
        "url": launch_response.Url,
        "lab_instance_id": launch_response.LabInstanceId,
        "expires": launch_response.Expires,
        "error": launch_response.Error,
        "status": launch_response.Status,
    }


class CourseLaunchAPIViewTestCase(LaunchAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:course-launch", args=[pk])

    def setUp(self):
        super().setUp()
        launch_output = LaunchResponse(Status=1, Error="", Result=0, Url="https://google.de", LabInstanceId=18, Expires=1000000000)
        self.launch_1_json = launch_json(launch_output)
        self.launch_course_service_mockup = LaunchCourseServiceMockup(launch_output)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILaunchCourseService, self.launch_course_service_mockup)

    def test_injected_mockup(self):
        course_view_set = CourseViewSet()
        self.assertIsInstance(course_view_set.launch_course_service, LaunchCourseServiceMockup)

    def test_launch_without_admin(self):
        """A person who is neither admin or has payed for a course."""
        response = self.client.post(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 403)

    def test_launch_with_payed_owner(self):
        """A person who is no admin but an owner of a company that has payed for a course."""
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.post(self.url(self.course_1.id))
        self.assertDictEqual(response.data, self.launch_1_json)
        self.assertEqual(response.status_code, 201)

    def test_launch_with_unpayed_owner(self):
        """A person who is no admin but an owner of a company that has payed for the wrong course."""
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.post(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 403)

    def test_launch_with_payed_member(self):
        """A person who is no admin but an member of a company that has payed for a course."""
        self.client.logout()
        self.login(self.member_1)
        response = self.client.post(self.url(self.course_1.id))
        self.assertDictEqual(response.data, self.launch_1_json)
        self.assertEqual(response.status_code, 201)

    def test_launch_with_unpayed_member(self):
        """A person who is no admin but an member of a company that has payed for a course."""
        self.client.logout()
        self.login(self.member_2)
        response = self.client.post(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 403)

    def test_launch_with_admin(self):
        """A person who is admin and has not payed for a course."""
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 403)

    def test_launch_without_login(self):
        """A person who is not authenticated."""
        self.client.logout()
        response = self.client.delete(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 401)

    def test_launch_get(self):
        response = self.client.get(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_patch(self):
        response = self.client.patch(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_put(self):
        response = self.client.put(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_delete(self):
        response = self.client.delete(self.url(self.course_1.id))
        self.assertEqual(response.status_code, 405)
