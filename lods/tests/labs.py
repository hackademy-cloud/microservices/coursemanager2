from typing import List

from django.urls import reverse
from lods_lib.api_methods.cancel import CancelResponse
from lods_lib.api_methods.resume import ResumeResponse
from lods_lib.api_methods.save import SaveResponse

from commons.dependency_injection import default_container
from lods import models
from lods.factories.lab_factory import ILabsFactory
from lods.services.destroy_lab_service import IDestroyLabService
from lods.services.resume_lab_service import IResumeLabService
from lods.services.save_lab_service import ISaveLabService
from lods.tests.utils import LabAPIViewTestCaseBase, labs_json
from lods.views import LabViewSet


class LabsFactoryMockup(ILabsFactory):
    def __init__(self, labs: List[models.Lab]):
        self.labs = labs

    def populate_data(self, user, disable_admin=False) -> List[models.Lab]:
        """Learn on demand systems filters the list automatically, so this should be done here too."""
        return list(filter(lambda lab: lab.user == user, self.labs))


class DestroyLabServiceMockup(IDestroyLabService):
    def __init__(self, cancel_response: CancelResponse):
        self.cancel_response = cancel_response

    def execute(self, instance: models.Lab) -> CancelResponse:
        return self.cancel_response


class ResumeLabServiceMockup(IResumeLabService):
    def __init__(self, resume_response: ResumeResponse):
        self.resume_response = resume_response

    def execute(self, instance: models.Lab):
        out = self.resume_response.__dict__
        out["LabInstanceId"] = instance.id
        return out


class SaveLabServiceMockup(IResumeLabService):
    def __init__(self, save_response: SaveResponse):
        self.save_response = save_response

    def execute(self, instance: models.Lab):
        out = self.save_response.__dict__
        out["LabInstanceId"] = instance.id
        return out


class LabListAPIViewTestCase(LabAPIViewTestCaseBase):
    url = reverse("lods:lab-list")

    def setUp(self):
        super().setUp()
        self.labs_factory_mockup = LabsFactoryMockup(self.labs)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILabsFactory, self.labs_factory_mockup)

    def test_injected_mockup(self):
        lab_view_set = LabViewSet()
        self.assertIsInstance(lab_view_set.labs_factory, LabsFactoryMockup)

    def test_with_admin(self):
        """Admins have no special rights to see others courses. They can look this up in
        learn on demand systems if they need it."""
        self.login_as_admin()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([]))

    def test_without_admin(self):
        """A person who is neither admin or has payed for a course."""
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([]))

    def test_with_payed_owner(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([self.lab_1]))

    def test_with_payed_member(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([self.lab_3]))

    def test_launch_without_login(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)

    def test_filter_match_running(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_running=True")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([self.lab_1]))

    def test_filter_unmatch_running(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_running=False")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([]))

    def test_filter_match_saved(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_saved=False")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([self.lab_1]))

    def test_filter_unmatch_saved(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_saved=True")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([]))

    def test_filter_match_others(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_others=False")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([self.lab_1]))

    def test_filter_unmatch_others(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url + "?is_others=True")
        self.assertEqual(response.status_code, 200)
        self.assertListEqual(response.data, labs_json([]))


class LabDetailAPIViewTestCase(LabAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:lab-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.labs_factory_mockup = LabsFactoryMockup(self.labs)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILabsFactory, self.labs_factory_mockup)

    def test_injected_mockup(self):
        lab_view_set = LabViewSet()
        self.assertIsInstance(lab_view_set.labs_factory, LabsFactoryMockup)

    def test_with_admin(self):
        self.login_as_admin()
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_without_admin(self):
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_owner(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, self.lab_1_json)

    def test_with_unpayed_owner(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_member(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.get(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, self.lab_3_json)

    def test_with_unpayed_member(self):
        self.client.logout()
        self.login(self.member_2)
        response = self.client.get(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 404)

    def test_launch_without_login(self):
        self.client.logout()
        response = self.client.get(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 401)


class LabDeleteAPIViewTestCase(LabAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:lab-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.labs_factory_mockup = LabsFactoryMockup(self.labs)
        self.cancel_response = CancelResponse(Result=1, Error=None, Status=1)
        self.destroy_lab_service_mockup = DestroyLabServiceMockup(self.cancel_response)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILabsFactory, self.labs_factory_mockup)
        default_container.register_instance(IDestroyLabService, self.destroy_lab_service_mockup)

    def test_injected_mockup(self):
        lab_view_set = LabViewSet()
        self.assertIsInstance(lab_view_set.labs_factory, LabsFactoryMockup)
        self.assertIsInstance(lab_view_set.destroy_lab_service, DestroyLabServiceMockup)

    def test_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_without_admin(self):
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_owner(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.data, None)

    def test_with_unpayed_owner(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_member(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.delete(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.data, None)

    def test_with_unpayed_member(self):
        self.client.logout()
        self.login(self.member_2)
        response = self.client.delete(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 404)

    def test_launch_without_login(self):
        self.client.logout()
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 401)


class LabResumeAPIViewTestCase(LabAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:lab-resume", args=[pk])

    def setUp(self):
        super().setUp()
        self.labs_factory_mockup = LabsFactoryMockup(self.labs)
        self.resume_response = ResumeResponse(Result=1, Error=None, Status=1, Url="test.com", Expires=12345)
        self.resume_lab_service_mockup = ResumeLabServiceMockup(self.resume_response)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILabsFactory, self.labs_factory_mockup)
        default_container.register_instance(IResumeLabService, self.resume_lab_service_mockup)

    def test_injected_mockup(self):
        lab_view_set = LabViewSet()
        self.assertIsInstance(lab_view_set.labs_factory, LabsFactoryMockup)
        self.assertIsInstance(lab_view_set.resume_lab_service, ResumeLabServiceMockup)

    def test_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_without_admin(self):
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_owner(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["lab_instance_id"], self.lab_1.id)

    def test_with_unpayed_owner(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_member(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.post(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["lab_instance_id"], self.lab_3.id)

    def test_with_unpayed_member(self):
        self.client.logout()
        self.login(self.member_2)
        response = self.client.post(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 404)

    def test_launch_without_login(self):
        self.client.logout()
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 401)

    def test_launch_get(self):
        self.login(self.owner_1)
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_patch(self):
        self.login(self.owner_1)
        response = self.client.patch(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_put(self):
        self.login(self.owner_1)
        response = self.client.put(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_delete(self):
        self.login(self.owner_1)
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)


class LabSaveAPIViewTestCase(LabAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:lab-save", args=[pk])

    def setUp(self):
        super().setUp()
        self.labs_factory_mockup = LabsFactoryMockup(self.labs)
        self.save_response = SaveResponse(Result=1, Error=None, Status=1, Expires=12345)
        self.save_lab_service_mockup = SaveLabServiceMockup(self.save_response)
        # overwrites the registration from lods.apps.LodsConfig.ready
        default_container.register_instance(ILabsFactory, self.labs_factory_mockup)
        default_container.register_instance(ISaveLabService, self.save_lab_service_mockup)

    def test_injected_mockup(self):
        lab_view_set = LabViewSet()
        self.assertIsInstance(lab_view_set.labs_factory, LabsFactoryMockup)
        self.assertIsInstance(lab_view_set.save_lab_service, SaveLabServiceMockup)

    def test_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_without_admin(self):
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_owner(self):
        self.client.logout()
        self.login(self.owner_1)
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["lab_instance_id"], self.lab_1.id)

    def test_with_unpayed_owner(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 404)

    def test_with_payed_member(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.post(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data["lab_instance_id"], self.lab_3.id)

    def test_with_unpayed_member(self):
        self.client.logout()
        self.login(self.member_2)
        response = self.client.post(self.url(self.lab_3.id))
        self.assertEqual(response.status_code, 404)

    def test_launch_without_login(self):
        self.client.logout()
        response = self.client.post(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 401)

    def test_launch_get(self):
        self.login(self.owner_1)
        response = self.client.get(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_patch(self):
        self.login(self.owner_1)
        response = self.client.patch(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_put(self):
        self.login(self.owner_1)
        response = self.client.put(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)

    def test_launch_delete(self):
        self.login(self.owner_1)
        response = self.client.delete(self.url(self.lab_1.id))
        self.assertEqual(response.status_code, 405)


class LabOthersAPIViewTestCase(LabAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:lab-detail", args=[pk])

    def test_launch_patch(self):
        self.login(self.owner_1)
        response = self.client.patch(self.url(self.lab_1))
        self.assertEqual(response.status_code, 405)

    def test_launch_put(self):
        self.login(self.owner_1)
        response = self.client.put(self.url(self.lab_1))
        self.assertEqual(response.status_code, 405)

