from django.urls import reverse
from rest_framework import serializers

from commons.utils_testing import TokenLoginAPITestCase
from lods.models import LabHistory, Catalog, Series, Course


def lab_history_json(lab_history: LabHistory):
    return {
        "id": lab_history.id,
        "user": lab_history.user.id,
        "course": lab_history.course.id,
        "action": lab_history.action,
        "datetime": serializers.DateTimeField().to_representation(lab_history.datetime),
        "status": lab_history.status,
        "error_message": lab_history.error_message
    }


class LabHistoryListAPIViewTestCase(TokenLoginAPITestCase):
    url = reverse("lods:lab_history-list")

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series = Series.objects.create(catalog=self.catalog, name="XSS")
        self.course = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger",
            enabled=True, series_id=self.series.id,
        )
        self.owner = self.create_user("pay_user_2@example.com", "geheim")
        self.lab_history = LabHistory.objects.create(
            user=self.owner, course=self.course, action="launch",
            status=1, error_message=None
        )

    def test_get_series_without_admin(self):
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 0)
        self.assertEqual(response.status_code, 200)

    def test_get_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_series_with_owner(self):
        self.client.logout()
        self.login(self.owner)
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)


class LabHistoryDetailAPIViewTestCase(TokenLoginAPITestCase):
    def url(self, pk):
        return reverse("lods:lab_history-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series = Series.objects.create(catalog=self.catalog, name="XSS")
        self.course = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger",
            enabled=True, series_id=self.series.id,
        )
        self.owner = self.create_user("pay_user_2@example.com", "geheim")
        self.lab_history = LabHistory.objects.create(
            user=self.owner, course=self.course, action="launch",
            status=1, error_message=None
        )
        self.lab_history_json = lab_history_json(self.lab_history)

    def test_get_series_without_admin(self):
        response = self.client.get(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 404)

    def test_get_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, self.lab_history_json)

    def test_get_series_with_owner(self):
        self.client.logout()
        self.login(self.owner)
        response = self.client.get(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, self.lab_history_json)

    def test_get_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 401)

    def test_post(self):
        response = self.client.post(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 405)

    def test_put(self):
        response = self.client.post(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 405)

    def test_patch(self):
        response = self.client.post(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 405)

    def test_delete(self):
        response = self.client.post(self.url(self.lab_history.id))
        self.assertEqual(response.status_code, 405)
