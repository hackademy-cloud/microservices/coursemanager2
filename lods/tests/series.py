from django.urls import reverse

from commons.utils_testing import TokenLoginAPITestCase
from lods.models import Series, Catalog
from lods.tests.utils import SeriesAndCourseAPIViewTestCaseBase


class SeriesListAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    url = reverse("lods:series-list")

    def test_get_series_without_admin(self):
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, 200)

    def test_get_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.status_code, 200)

    def test_get_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)


class SeriesDetailAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):

    def url(self, pk):
        return reverse("lods:series-detail", args=[pk])

    def test_get_series_without_admin(self):
        response = self.client.get(self.url(self.series_1.id))
        self.assertDictEqual(response.data, self.series_1_json)
        self.assertEqual(response.status_code, 200)

    def test_get_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url(self.series_1.id))
        self.assertDictEqual(response.data, self.series_1_json)
        self.assertEqual(response.status_code, 200)

    def test_get_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url(self.series_1.id))
        self.assertEqual(response.status_code, 401)


class SeriesPostAPIViewTestCase(TokenLoginAPITestCase):
    url = reverse("lods:series-list")

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_post_series_without_admin(self):
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Series.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(Series.objects.all()), 0)

    def test_post_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Series.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(Series.objects.all()), 0)

    def test_post_series_without_login(self):
        self.client.logout()
        response = self.client.post(self.url, data={
            "name": "XSS", "description": "XSS Kurs", "catalog_id": self.catalog.id
        })
        self.assertEqual(len(Series.objects.all()), 0)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(len(Series.objects.all()), 0)


class SeriesPutAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_put_series_without_admin(self):
        response = self.client.put(self.url(self.series_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 405)

    def test_put_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.put(self.url(self.series_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 405)

    def test_put_series_without_login(self):
        self.client.logout()
        response = self.client.put(self.url(self.series_1.id), data={
            "name": "XSS 2", "description": "XSS Kurs 2", "catalog_id": self.catalog.id
        })
        self.assertEqual(response.status_code, 401)


class SeriesPatchAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_patch_series_without_admin(self):
        response = self.client.patch(self.url(self.series_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 405)

    def test_patch_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.patch(self.url(self.series_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 405)

    def test_patch_series_without_login(self):
        self.client.logout()
        response = self.client.patch(self.url(self.series_1.id), data={"name": "XSS 2"})
        self.assertEqual(response.status_code, 401)


class SeriesDeleteAPIViewTestCase(SeriesAndCourseAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("lods:series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_delete_series_without_admin(self):
        response = self.client.delete(self.url(self.series_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.delete(self.url(self.series_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_series_without_login(self):
        self.client.logout()
        response = self.client.delete(self.url(self.series_1.id))
        self.assertEqual(response.status_code, 401)
