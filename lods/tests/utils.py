from typing import List

from commons.utils_testing import TokenLoginAPITestCase
from company.models import Company, Membership
from lods.models import Course, Series, Catalog, Lab
from payed_series.models import PayedSeries


def series_course_json(course: Course):
    return {
        "id": course.id,
        "name": course.name,
        "enabled": course.enabled,
        "reason_disabled": course.reason_disabled,
        "description": course.description,
        "expected_duration_minutes": course.expected_duration_minutes,
        "duration_minutes": course.duration_minutes,
        "tags": course.tags
    }


def series_courses_json(courses: List[Course]):
    return [series_course_json(course) for course in courses]


def series_json(series: Series):
    return {
        "id": series.id,
        "name": series.name,
        "description": series.description,
        "num_training_days": series.num_training_days,
        "courses": series_courses_json(series.courses.all())
    }


def course_series_json(series: Series):
    return {
        "id": series.id,
        "name": series.name,
        "description": series.description,
        "num_training_days": series.num_training_days
    }


def course_json(course: Course):
    return {
        "id": course.id,
        "name": course.name,
        "platform_id": course.platform_id,
        "enabled": course.enabled,
        "number": course.number,
        "reason_disabled": course.reason_disabled,
        "development_status_id": course.development_status_id,
        "description": course.description,
        "expected_duration_minutes": course.expected_duration_minutes,
        "duration_minutes": course.duration_minutes,
        "tags": course.tags,
        "content_version": course.content_version,
        "series": course_series_json(course.series)
    }


class SeriesAndCourseAPIViewTestCaseBase(TokenLoginAPITestCase):
    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series_1 = Series.objects.create(
            catalog=self.catalog,
            name="XSS",
            description="Hier lernt man XSS",
            num_training_days=1)
        self.series_2 = Series.objects.create(
            catalog=self.catalog,
            name="Buffer Overflow",
            description="Hier lernt man Metasploit",
            num_training_days=1)
        self.course_1 = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger", number="1-xss-anfaenger",
            platform_id=2, cloud_platform_id=None, enabled=True, reason_disabled=None,
            development_status_id=10,
            description="XSS wird an einem Beispiel gezeigt",
            series_id=self.series_1.id,
            objective="", scenario="",
            expected_duration_minutes=10, duration_minutes=5,
            ram=1000,
            has_integrated_content=False, content_version=6, is_exam=False,
            premium_price=7, basic_price=8,
            tags=["Ubuntu", "XSS", "Webdev"],
            shared_class_environment_role_id=0,
        )
        self.course_2 = Course.objects.create(
            catalog=self.catalog, name="XSS für Profis", number="2-xss-profis",
            platform_id=2, cloud_platform_id=None, enabled=True, reason_disabled=None,
            development_status_id=10,
            description="XSS wird angewendet",
            series_id=self.series_1.id,
            objective="", scenario="",
            expected_duration_minutes=20, duration_minutes=15,
            ram=1000,
            has_integrated_content=False, content_version=6, is_exam=False,
            premium_price=7, basic_price=8,
            tags=["Ubuntu", "XSS", "Webdev"],
            shared_class_environment_role_id=0,
        )
        self.course_3 = Course.objects.create(
            catalog=self.catalog, name="Metasploit", number="3-meta",
            platform_id=2, cloud_platform_id=None, enabled=True, reason_disabled=None,
            development_status_id=10,
            description="Es wird der Umgang mit Metasploit gezeigt",
            series_id=self.series_2.id,
            objective="", scenario="",
            expected_duration_minutes=20, duration_minutes=15,
            ram=1000,
            has_integrated_content=False, content_version=6, is_exam=False,
            premium_price=7, basic_price=8,
            tags=["Debian", "Metasploit", "Buffer Overflow"],
            shared_class_environment_role_id=0,
        )
        self.series_1_json = series_json(self.series_1)
        self.series_2_json = series_json(self.series_2)
        self.course_1_json = course_json(self.course_1)
        self.course_2_json = course_json(self.course_2)
        self.course_3_json = course_json(self.course_3)


class LaunchAPIViewTestCaseBase(TokenLoginAPITestCase):
    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series_1 = Series.objects.create(
            catalog=self.catalog, name="XSS"
        )
        self.series_2 = Series.objects.create(
            catalog=self.catalog, name="Buffer Overflow"
        )
        self.course_1 = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger",
            enabled=True, series_id=self.series_1.id,
        )
        self.course_2 = Course.objects.create(
            catalog=self.catalog, name="Buffer für Anfänger",
            enabled=True, series_id=self.series_2.id,
        )
        self.owner_1 = self.create_user("pay_user_2@example.com", "geheim")
        self.owner_2 = self.create_user("pay_user_2_2@example.com", "geheim")
        self.member_1 = self.create_user("pay_user_3@example.com", "geheim")
        self.member_2 = self.create_user("pay_user_4@example.com", "geheim")
        self.company_1 = Company.objects.create(name="Google", owner=self.owner_1)
        self.company_2 = Company.objects.create(name="Facebook", owner=self.owner_2)
        self.membership_1 = Membership.objects.create(user=self.member_1, company=self.company_1)
        self.membership_2 = Membership.objects.create(user=self.member_2, company=self.company_2)
        self.payed_series_1 = PayedSeries.objects.create(company=self.company_1, series=self.series_1)
        self.payed_series_2 = PayedSeries.objects.create(company=self.company_2, series=self.series_2)
        # Owner 1 is owner of series 1 and has access to course 1
        # Member 1 is member in series 1 and has access to course 1
        # Owner 2 is owner of series 2 and has access to course 2
        # Member 2 is member in series 2 and has access to course 2


def lab_json(lab: Lab):
    return {
        'id': lab.id,
        'user': {
            'id': lab.user.id,
            'email': lab.user.email,
            'first_name': lab.user.first_name,
            'last_name': lab.user.last_name
        },
        'course': {
            'id': lab.course.id,
            'name': lab.course.name,
            'number': lab.course.number
        },
        'minutes_remaining': lab.minutes_remaining,
        'saved': lab.saved,
        'start': lab.start,
        'expires': lab.expires,
        'save_in_progress': lab.save_in_progress,
        'url': lab.url,
        'is_running': lab.is_running,
        'is_saved': lab.is_saved,
        'is_others': lab.is_others
    }


def labs_json(labs: List[Lab]):
    return [lab_json(lab) for lab in labs]


class LabAPIViewTestCaseBase(TokenLoginAPITestCase):
    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series_1 = Series.objects.create(
            catalog=self.catalog, name="XSS"
        )
        self.series_2 = Series.objects.create(
            catalog=self.catalog, name="Buffer Overflow"
        )
        self.course_1 = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger",
            enabled=True, series_id=self.series_1.id,
        )
        self.course_2 = Course.objects.create(
            catalog=self.catalog, name="Buffer für Anfänger",
            enabled=True, series_id=self.series_2.id,
        )
        self.owner_1 = self.create_user("pay_user_2@example.com", "geheim")
        self.owner_2 = self.create_user("pay_user_2_2@example.com", "geheim")
        self.member_1 = self.create_user("pay_user_3@example.com", "geheim")
        self.member_2 = self.create_user("pay_user_4@example.com", "geheim")
        self.company_1 = Company.objects.create(name="Google", owner=self.owner_1)
        self.company_2 = Company.objects.create(name="Facebook", owner=self.owner_2)
        self.membership_1 = Membership.objects.create(user=self.member_1, company=self.company_1)
        self.membership_2 = Membership.objects.create(user=self.member_2, company=self.company_2)
        self.payed_series_1 = PayedSeries.objects.create(company=self.company_1, series=self.series_1)
        self.payed_series_2 = PayedSeries.objects.create(company=self.company_2, series=self.series_2)
        # Owner 1 is owner of series 1 and has access to course 1
        # Member 1 is member in series 1 and has access to course 1
        # Owner 2 is owner of series 2 and has access to course 2
        # Member 2 is member in series 2 and has access to course 2
        self.lab_1 = Lab(id=12345, user=self.owner_1, course=self.course_1, is_running=True, is_others=False, is_saved=False)
        self.lab_2 = Lab(id=12346, user=self.owner_2, course=self.course_2, is_running=True, is_others=False, is_saved=False)
        self.lab_3 = Lab(id=12347, user=self.member_1, course=self.course_1, is_running=True, is_others=False, is_saved=False)
        self.lab_4 = Lab(id=12348, user=self.member_2, course=self.course_2, is_running=True, is_others=False, is_saved=False)
        self.labs = [self.lab_1, self.lab_2, self.lab_3, self.lab_4]
        self.lab_1_json = lab_json(self.lab_1)
        self.lab_2_json = lab_json(self.lab_2)
        self.lab_3_json = lab_json(self.lab_3)
        self.lab_4_json = lab_json(self.lab_4)
        self.labs_json = labs_json(self.labs)
