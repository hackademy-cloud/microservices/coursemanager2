from typing import Optional

from django.test import testcases
from lods_lib.api_methods.catalog import CatalogResponse, CatalogParameters
from lods_lib.client import ILodsApiClient
from lods_lib.datamodel.lab_profile import LabProfile
from lods_lib.datamodel.lab_series import LabSeries as LabSeriesAlias

from lods import models
from lods.factories.catalog_factory import CourseFactory, SeriesFactory, CatalogFactory


example_1_catalog_ret = CatalogResponse(
    LabSeries=[
        LabSeriesAlias(Id=8821, Name="XSS Serie"),
        LabSeriesAlias(Id=9223, Name="Buffer Serie")
    ],
    LabProfiles=[
        LabProfile(Id=3923, Name="XSS", SeriesId=8821),
        LabProfile(Id=1245, Name="Buffer Overflow", SeriesId=9223)
    ],
    DeliveryRegions=[],
    Error=None,
    Status=1
)


example_2_catalog_ret = CatalogResponse(
    LabSeries=[],
    LabProfiles=[],
    DeliveryRegions=[],
    Error="Some error",
    Status=0
)


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, catalog_ret: CatalogResponse):
        self.catalog_ret = catalog_ret

    def catalog(self, params: Optional[CatalogParameters]=None) -> CatalogResponse:
        return self.catalog_ret


class SeriesFactoryTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = models.Catalog.objects.create(error=None, status=1)
        self.series_factory = SeriesFactory(self.catalog)

    def test_multi_update(self):
        self.assertEqual(len(models.Series.objects.all()), 0)
        # perform update
        self.series_factory.multi_update(example_1_catalog_ret.LabSeries)  # asserting this works
        # test if update worked
        self.assertEqual(len(models.Series.objects.all()), 2)
        series_1 = models.Series.objects.get(pk=8821)
        self.assertEqual(series_1.id, 8821)
        self.assertEqual(series_1.name, "XSS Serie")
        series_2 = models.Series.objects.get(pk=9223)
        self.assertEqual(series_2.id, 9223)
        self.assertEqual(series_2.name, "Buffer Serie")

    def test_multi_update_each_key(self):
        old_objs = [
            LabSeriesAlias(
                Id=1,
                Name="hi",
                Description="Hallo",
                NumTrainingDays=8
            )
        ]
        self.assertEqual(len(models.Series.objects.all()), 0)
        # perform update
        self.series_factory.multi_update(old_objs)
        # test if update worked
        self.assertEqual(len(models.Series.objects.all()), 1)
        series_1 = models.Series.objects.get(pk=1)
        self.assertEqual(series_1.id, 1)
        self.assertEqual(series_1.name, "hi")
        self.assertEqual(series_1.description, "Hallo")
        self.assertEqual(series_1.num_training_days, 8)
        self.assertEqual(len(series_1.courses.all()), 0)


class CourseFactoryTestCase(testcases.TestCase):
    def setUp(self):
        self.catalog = models.Catalog.objects.create(error=None, status=1)
        self.series_factory = SeriesFactory(self.catalog)
        self.course_factory = CourseFactory(self.catalog)

    def test_multi_update(self):
        self.assertEqual(len(models.Course.objects.all()), 0)
        # perform update
        self.series_factory.multi_update(example_1_catalog_ret.LabSeries)  # asserting this works
        self.course_factory.multi_update(example_1_catalog_ret.LabProfiles)
        # test if update worked
        self.assertEqual(len(models.Course.objects.all()), 2)
        course_1 = models.Course.objects.get(pk=3923)
        self.assertEqual(course_1.id, 3923)
        self.assertEqual(course_1.name, "XSS")
        course_2 = models.Course.objects.get(pk=1245)
        self.assertEqual(course_2.id, 1245)
        self.assertEqual(course_2.name, "Buffer Overflow")

    def test_multi_update_each_key(self):
        old_objs = [
            LabProfile(
                Id=1,
                Name="Hi",
                Number="hi-1",
                PlatformId=2,
                CloudPlatformId=3,
                Enabled=True,
                ReasonDisabled="No reason",
                DevelopmentStatusId=4,
                Description="Description",
                SeriesId=None,
                Objective="Objective",
                Scenario="Scenario",
                ExpectedDurationMinutes=6,
                DurationMinutes=7,
                RAM=100,
                HasIntegratedContent=False,
                ContentVersion=3,
                IsExam=False,
                PremiumPrice=7.89,
                BasicPrice=8.79,
                Tags=["Ubuntu", "Debian"],
                SharedClassEnvironmentRoleId=6
            )]
        self.assertEqual(len(models.Course.objects.all()), 0)
        # perform update
        self.course_factory.multi_update(old_objs)
        # test if update worked
        self.assertEqual(len(models.Course.objects.all()), 1)
        course_1 = models.Course.objects.get(pk=1)
        self.assertEqual(course_1.id, 1)
        self.assertEqual(course_1.name, "Hi")
        self.assertEqual(course_1.number, "hi-1")
        self.assertEqual(course_1.platform_id, 2)
        self.assertEqual(course_1.cloud_platform_id, 3)
        self.assertEqual(course_1.enabled, True)
        self.assertEqual(course_1.reason_disabled, "No reason")
        self.assertEqual(course_1.development_status_id, 4)
        self.assertEqual(course_1.description, "Description")
        self.assertEqual(course_1.series_id, None)
        self.assertEqual(course_1.objective, "Objective")
        self.assertEqual(course_1.scenario, "Scenario")
        self.assertEqual(course_1.expected_duration_minutes, 6)
        self.assertEqual(course_1.duration_minutes, 7)
        self.assertEqual(course_1.ram, 100)
        self.assertEqual(course_1.has_integrated_content, False)
        self.assertEqual(course_1.content_version, 3)
        self.assertEqual(course_1.is_exam, False)
        self.assertEqual(course_1.premium_price, 7.89)
        self.assertEqual(course_1.basic_price, 8.79)
        self.assertListEqual(course_1.tags, ["Ubuntu", "Debian"])
        self.assertEqual(course_1.shared_class_environment_role_id, 6)


class CatalogFactoryTestCase(testcases.TestCase):

    def test_populate_data(self):
        lods_api_client = LodsApiClientMockup(example_1_catalog_ret)
        catalog_factory = CatalogFactory(lods_api_client=lods_api_client, catalog_id=10)
        self.assertEqual(len(models.Catalog.objects.all()), 0)
        self.assertEqual(len(models.Series.objects.all()), 0)
        self.assertEqual(len(models.Course.objects.all()), 0)
        catalog = catalog_factory.populate_data()
        # test if update worked
        self.assertEqual(len(models.Catalog.objects.all()), 1)
        self.assertEqual(len(models.Series.objects.all()), 2)
        self.assertEqual(len(models.Course.objects.all()), 2)
        self.assertEqual(catalog.id, 10)
        self.assertEqual(catalog.error, None)
        self.assertEqual(catalog.status, 1)
        series = models.Series.objects.all()
        for serie in series:
            self.assertEqual(serie.catalog, catalog)
        courses = models.Course.objects.all()
        for course in courses:
            self.assertEqual(course.catalog, catalog)

    def test_populate_data_with_error(self):
        lods_api_client = LodsApiClientMockup(example_2_catalog_ret)
        catalog_factory = CatalogFactory(lods_api_client=lods_api_client, catalog_id=4)
        self.assertEqual(len(models.Catalog.objects.all()), 0)
        self.assertEqual(len(models.Series.objects.all()), 0)
        self.assertEqual(len(models.Course.objects.all()), 0)
        catalog_factory.populate_data()
        # test if update worked
        self.assertEqual(len(models.Catalog.objects.all()), 1)
        self.assertEqual(len(models.Series.objects.all()), 0)
        self.assertEqual(len(models.Course.objects.all()), 0)
        catalog = models.Catalog.objects.first()
        self.assertEqual(catalog.error, "Some error")
        self.assertEqual(catalog.status, 0)
