from typing import Optional, List

from django.contrib.auth import get_user_model
from django.test import testcases
from lods_lib.api_methods.running_and_saved_labs import RunningAndSavedLabsResponse
from lods_lib.api_methods.user_running_and_saved_labs import UserRunningAndSavedLabsParameters, \
    UserRunningAndSavedLabsResponse
from lods_lib.client import ILodsApiClient
from lods_lib.datamodel.lab_instance import LabInstance
from lods_lib.datamodel.running_lab import RunningLab
from lods_lib.datamodel.saved_lab import SavedLab

from lods.exceptions import UserRunningAndSavedLabsError
from lods.factories.lab_factory import RunningLabFactory, SavedLabFactory, LabInstanceFactory, LabsFactory
from lods.models import Course, Series, Lab, Catalog


# TODO: this code is crap


def example_running_labs(self):
    return [
        RunningLab(
            LabInstanceId=1,
            LabProfileId=self.course1.id,
            LabProfileName=self.course1.name,
            LabProfileNumber=self.course1.number,
            Start=1617572464,
            Expires=1617579464,
            Url="https://google.de/bla",
            IsExam=False
        ),
        RunningLab(
            LabInstanceId=2,
            LabProfileId=self.course2.id,
            LabProfileName=self.course2.name,
            LabProfileNumber=self.course2.number,
            Start=1617572479,
            Expires=1617579488,
            Url="https://google.de/bla2",
            IsExam=False
        ),
    ]


def test_running_lab(self, lab_1, lab_2):
    self.assertEqual(lab_1.id, 1)
    self.assertEqual(lab_1.user, self.user_1)
    self.assertEqual(lab_1.course, self.course1)
    self.assertEqual(lab_1.minutes_remaining, None)
    self.assertEqual(lab_1.saved, None)
    self.assertEqual(lab_1.start, self.running_labs[0].Start)
    self.assertEqual(lab_1.expires, self.running_labs[0].Expires)
    self.assertFalse(lab_1.save_in_progress)
    self.assertEqual(lab_1.url, self.running_labs[0].Url)
    self.assertTrue(lab_1.is_running)
    self.assertFalse(lab_1.is_saved)
    self.assertFalse(lab_1.is_others)
    self.assertEqual(lab_2.id, 2)
    self.assertEqual(lab_2.user, self.user_1)
    self.assertEqual(lab_2.course, self.course2)
    self.assertEqual(lab_2.minutes_remaining, None)
    self.assertEqual(lab_2.saved, None)
    self.assertEqual(lab_2.start, self.running_labs[1].Start)
    self.assertEqual(lab_2.expires, self.running_labs[1].Expires)
    self.assertFalse(lab_2.save_in_progress)
    self.assertEqual(lab_2.url, self.running_labs[1].Url)
    self.assertTrue(lab_2.is_running)
    self.assertFalse(lab_2.is_saved)
    self.assertFalse(lab_2.is_others)


def example_saved_labs(self):
    return [
        SavedLab(
            LabInstanceId=3,
            LabProfileId=self.course1.id,
            LabProfileName=self.course1.name,
            LabProfileNumber=self.course1.number,
            MinutesRemaining=50,
            Saved=1617579063,
            Expires=1617579464,
            SaveInProgress=False,
            IsExam=False,
            SubmittedForGrading=False,
        ),
        SavedLab(
            LabInstanceId=4,
            LabProfileId=self.course2.id,
            LabProfileName=self.course2.name,
            LabProfileNumber=self.course2.number,
            MinutesRemaining=44,
            Saved=1617579058,
            Expires=1617579459,
            SaveInProgress=False,
            IsExam=False,
            SubmittedForGrading=False,
        ),
    ]


def test_saved_lab(self, lab_1, lab_2):
    self.assertEqual(lab_1.id, 3)
    self.assertEqual(lab_1.user, self.user_1)
    self.assertEqual(lab_1.course, self.course1)
    self.assertEqual(lab_1.minutes_remaining, self.saved_labs[0].MinutesRemaining)
    self.assertEqual(lab_1.saved, self.saved_labs[0].Saved)
    self.assertEqual(lab_1.start, None)
    self.assertEqual(lab_1.expires, self.saved_labs[0].Expires)
    self.assertFalse(lab_1.save_in_progress)
    self.assertEqual(lab_1.url, None)
    self.assertFalse(lab_1.is_running)
    self.assertTrue(lab_1.is_saved)
    self.assertFalse(lab_1.is_others)
    self.assertEqual(lab_2.id, 4)
    self.assertEqual(lab_2.user, self.user_1)
    self.assertEqual(lab_2.course, self.course2)
    self.assertEqual(lab_2.minutes_remaining, self.saved_labs[1].MinutesRemaining)
    self.assertEqual(lab_2.saved, self.saved_labs[1].Saved)
    self.assertEqual(lab_2.start, None)
    self.assertEqual(lab_2.expires, self.saved_labs[1].Expires)
    self.assertFalse(lab_2.save_in_progress)
    self.assertEqual(lab_2.url, None)
    self.assertFalse(lab_2.is_running)
    self.assertTrue(lab_2.is_saved)
    self.assertFalse(lab_2.is_others)


def example_lab_instances(self):
    return [
        LabInstance(
            Id=20,
            UserId=self.user_1.id,
            UserFirstName=self.user_1.first_name,
            UserLastName=self.user_1.last_name,
            LabProfileId=self.course1.id,
            LabProfileNumber=self.course1.number,
            LabProfileName=self.course1.name,
            Start=1617572464,
            Expires=1617579464,
            IsExam=False
        ),
        LabInstance(
            Id=22,
            UserId=self.user_2.id,
            UserFirstName=self.user_2.first_name,
            UserLastName=self.user_2.last_name,
            LabProfileId=self.course2.id,
            LabProfileNumber=self.course2.number,
            LabProfileName=self.course2.name,
            Start=1617572482,
            Expires=1617579477,
            IsExam=False
        ),
    ]


def test_lab_instance(self, lab_1, lab_2):
    self.assertEqual(lab_1.id, self.lab_instances[0].Id)
    self.assertEqual(lab_1.user, self.user_1)
    self.assertEqual(lab_1.course, self.course1)
    self.assertEqual(lab_1.minutes_remaining, None)
    self.assertEqual(lab_1.saved, None)
    self.assertEqual(lab_1.start, self.lab_instances[0].Start)
    self.assertEqual(lab_1.expires, self.lab_instances[0].Expires)
    self.assertFalse(lab_1.save_in_progress)
    self.assertEqual(lab_1.url, None)
    self.assertFalse(lab_1.is_running)
    self.assertFalse(lab_1.is_saved)
    self.assertFalse(lab_1.is_others)
    self.assertEqual(lab_2.id, self.lab_instances[1].Id)
    self.assertEqual(lab_2.user, self.user_2)
    self.assertEqual(lab_2.course, self.course2)
    self.assertEqual(lab_2.minutes_remaining, None)
    self.assertEqual(lab_2.saved, None)
    self.assertEqual(lab_2.start, self.lab_instances[1].Start)
    self.assertEqual(lab_2.expires, self.lab_instances[1].Expires)
    self.assertFalse(lab_2.save_in_progress)
    self.assertEqual(lab_2.url, None)
    self.assertFalse(lab_2.is_running)
    self.assertFalse(lab_2.is_saved)
    self.assertTrue(lab_2.is_others)


class LodsApiClientMockup(ILodsApiClient):
    def __init__(self, running_and_saved_labs_response: RunningAndSavedLabsResponse,
                 user_running_and_saved_labs_response: UserRunningAndSavedLabsResponse):
        self.running_and_saved_labs_response = running_and_saved_labs_response
        self.user_running_and_saved_labs_response = user_running_and_saved_labs_response

    def running_and_saved_labs(self) -> RunningAndSavedLabsResponse:
        return self.running_and_saved_labs_response

    def user_running_and_saved_labs(self, params: Optional[UserRunningAndSavedLabsParameters] = None) -> UserRunningAndSavedLabsResponse:
        return self.user_running_and_saved_labs_response


class RunningLabFactoryTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course1 = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                             catalog=self.catalog)
        self.course2 = Course.objects.create(id=2189, name="Course 2", number="course-2189", series_id=self.series.id,
                                             catalog=self.catalog)
        self.user_1 = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.running_lab_factory = RunningLabFactory(self.user_1)

    def test_multi_update(self):
        self.running_labs = example_running_labs(self)
        running_labs: List[Lab] = self.running_lab_factory.multi_update(self.running_labs)
        self.assertEqual(len(running_labs), 2)
        lab_1 = list(filter(lambda x: x.id == 1, running_labs))[0]
        lab_2 = list(filter(lambda x: x.id == 2, running_labs))[0]
        test_running_lab(self, lab_1, lab_2)

    def test_multi_update_wrong_course_id(self):
        input_labs = [RunningLab(
                LabInstanceId=51,
                LabProfileId=10000,
                LabProfileName=self.course2.name,
                LabProfileNumber=self.course2.number,
                Start=1617572479,
                Expires=1617579488,
                Url="https://google.de/bla2",
                IsExam=False
            )]
        running_labs = self.running_lab_factory.multi_update(input_labs)
        lab_1 = list(filter(lambda x: x.id == 51, running_labs))[0]
        self.assertEqual(lab_1.course, None)


class SavedLabFactoryTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course1 = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                             catalog=self.catalog)
        self.course2 = Course.objects.create(id=2189, name="Course 2", number="course-2189", series_id=self.series.id,
                                             catalog=self.catalog)
        self.user_1 = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.saved_lab_factory = SavedLabFactory(self.user_1)

    def test_multi_update(self):
        self.saved_labs = example_saved_labs(self)
        saved_labs: List[Lab] = self.saved_lab_factory.multi_update(self.saved_labs)
        self.assertEqual(len(saved_labs), 2)
        lab_1 = list(filter(lambda x: x.id == 3, saved_labs))[0]
        lab_2 = list(filter(lambda x: x.id == 4, saved_labs))[0]
        test_saved_lab(self, lab_1, lab_2)

    def test_multi_update_wrong_course_id(self):
        input_labs = [SavedLab(
            LabInstanceId=50,
            LabProfileId=10000,
            LabProfileName=self.course1.name,
            LabProfileNumber=self.course1.number,
            MinutesRemaining=50,
            Saved=1617579063,
            Expires=1617579464,
            SaveInProgress=False,
            IsExam=False,
            SubmittedForGrading=False,
        )]
        saved_labs = self.saved_lab_factory.multi_update(input_labs)
        lab_1 = list(filter(lambda x: x.id == 50, saved_labs))[0]
        self.assertEqual(lab_1.course, None)


class LabInstanceFactoryTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course1 = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                             catalog=self.catalog)
        self.course2 = Course.objects.create(id=2189, name="Course 2", number="course-2189", series_id=self.series.id,
                                             catalog=self.catalog)
        self.user_1 = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.user_2 = get_user_model().objects.create_user(email="test_user2@mail.de", password="geheim")
        self.lab_instance_factory = LabInstanceFactory(self.user_1)

    def test_multi_update(self):
        self.lab_instances = example_lab_instances(self)
        lab_instances: List[Lab] = self.lab_instance_factory.multi_update(self.lab_instances)
        self.assertEqual(len(lab_instances), 2)
        lab_1 = list(filter(lambda x: x.id == 20, lab_instances))[0]
        lab_2 = list(filter(lambda x: x.id == 22, lab_instances))[0]
        test_lab_instance(self, lab_1, lab_2)

    def test_multi_update_wrong_user_id(self):
        input_labs = [LabInstance(
            Id=52,
            UserId="10000",
            UserFirstName=self.user_2.first_name,
            UserLastName=self.user_2.last_name,
            LabProfileId=self.course2.id,
            LabProfileNumber=self.course2.number,
            LabProfileName=self.course2.name,
            Start=1617572482,
            Expires=1617579477,
            IsExam=False
        )]
        lab_instances = self.lab_instance_factory.multi_update(input_labs)
        lab_1 = list(filter(lambda x: x.id == 52, lab_instances))[0]
        self.assertEqual(lab_1.user, None)

    def test_multi_update_wrong_course_id(self):
        input_labs = [LabInstance(
            Id=53,
            UserId=self.user_2.id,
            UserFirstName=self.user_2.first_name,
            UserLastName=self.user_2.last_name,
            LabProfileId=10000,
            LabProfileNumber=self.course2.number,
            LabProfileName=self.course2.name,
            Start=1617572482,
            Expires=1617579477,
            IsExam=False
        )]
        lab_instances = self.lab_instance_factory.multi_update(input_labs)
        lab_1 = list(filter(lambda x: x.id == 53, lab_instances))[0]
        self.assertEqual(lab_1.course, None)


class LabsFactoryTestCase(testcases.TestCase):
    def setUp(self) -> None:
        self.catalog = Catalog.objects.create()
        self.series = Series.objects.create(id=1234, name="Series", catalog=self.catalog)
        self.course1 = Course.objects.create(id=2188, name="Course", number="course-2188", series_id=self.series.id,
                                             catalog=self.catalog)
        self.course2 = Course.objects.create(id=2189, name="Course 2", number="course-2189", series_id=self.series.id,
                                             catalog=self.catalog)
        self.user_1 = get_user_model().objects.create_user(email="test_user@mail.de", password="geheim")
        self.user_2 = get_user_model().objects.create_user(email="test_user2@mail.de", password="geheim")

    def test_multi_update_non_staff(self):
        self.lab_instances = example_lab_instances(self)
        admin_labs = RunningAndSavedLabsResponse(
            RunningLabs=self.lab_instances,
            SavedLabs=None,
            Error="",
            Status=1,
        )
        self.running_labs = example_running_labs(self)
        self.saved_labs = example_saved_labs(self)
        user_labs = UserRunningAndSavedLabsResponse(
            RunningLabs=self.running_labs,
            SavedLabs=self.saved_labs,
            Error="",
            Status=1,
        )
        lods_api_client = LodsApiClientMockup(admin_labs, user_labs)
        labs_factory = LabsFactory(lods_api_client=lods_api_client)
        labs = labs_factory.populate_data(self.user_1)
        self.assertEqual(len(labs), 4)
        lab_1 = list(filter(lambda x: x.id == 1, labs))[0]
        lab_2 = list(filter(lambda x: x.id == 2, labs))[0]
        lab_3 = list(filter(lambda x: x.id == 3, labs))[0]
        lab_4 = list(filter(lambda x: x.id == 4, labs))[0]
        test_running_lab(self, lab_1, lab_2)
        test_saved_lab(self, lab_3, lab_4)

    def test_multi_update_admin(self):
        self.user_1.is_staff = True
        self.lab_instances = example_lab_instances(self)
        admin_labs = RunningAndSavedLabsResponse(
            RunningLabs=self.lab_instances,
            SavedLabs=[],
            Error="",
            Status=1,
        )
        self.running_labs = example_running_labs(self)
        self.saved_labs = example_saved_labs(self)
        user_labs = UserRunningAndSavedLabsResponse(
            RunningLabs=self.running_labs,
            SavedLabs=self.saved_labs,
            Error="",
            Status=1,
        )
        lods_api_client = LodsApiClientMockup(admin_labs, user_labs)
        labs_factory = LabsFactory(lods_api_client=lods_api_client)
        labs = labs_factory.populate_data(self.user_1)
        self.assertEqual(len(labs), 6)
        lab_1 = list(filter(lambda x: x.id == 1, labs))[0]
        lab_2 = list(filter(lambda x: x.id == 2, labs))[0]
        lab_3 = list(filter(lambda x: x.id == 3, labs))[0]
        lab_4 = list(filter(lambda x: x.id == 4, labs))[0]
        lab_5 = list(filter(lambda x: x.id == 20, labs))[0]
        lab_6 = list(filter(lambda x: x.id == 22, labs))[0]
        test_running_lab(self, lab_1, lab_2)
        test_saved_lab(self, lab_3, lab_4)
        test_lab_instance(self, lab_5, lab_6)

    def test_multi_update_admin_error(self):
        self.user_1.is_staff = True
        self.lab_instances = example_lab_instances(self)
        admin_labs = RunningAndSavedLabsResponse(
            RunningLabs=self.lab_instances,
            SavedLabs=[],
            Error="An Error",
            Status=0,
        )
        self.running_labs = example_running_labs(self)
        self.saved_labs = example_saved_labs(self)
        user_labs = UserRunningAndSavedLabsResponse(
            RunningLabs=self.running_labs,
            SavedLabs=self.saved_labs,
            Error="",
            Status=1,
        )
        lods_api_client = LodsApiClientMockup(admin_labs, user_labs)
        labs_factory = LabsFactory(lods_api_client=lods_api_client)
        with self.assertRaises(UserRunningAndSavedLabsError):
            labs_factory.populate_data(self.user_1)

    def test_multi_update_user_error(self):
        self.user_1.is_staff = True
        self.lab_instances = example_lab_instances(self)
        admin_labs = RunningAndSavedLabsResponse(
            RunningLabs=self.lab_instances,
            SavedLabs=[],
            Error="",
            Status=1,
        )
        self.running_labs = example_running_labs(self)
        self.saved_labs = example_saved_labs(self)
        user_labs = UserRunningAndSavedLabsResponse(
            RunningLabs=self.running_labs,
            SavedLabs=self.saved_labs,
            Error="An Error",
            Status=0,
        )
        lods_api_client = LodsApiClientMockup(admin_labs, user_labs)
        labs_factory = LabsFactory(lods_api_client=lods_api_client)
        with self.assertRaises(UserRunningAndSavedLabsError):
            labs_factory.populate_data(self.user_1)
