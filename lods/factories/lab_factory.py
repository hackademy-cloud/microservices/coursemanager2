from typing import List, Any

from django.contrib.auth import get_user_model
from lods_lib.api_methods.user_running_and_saved_labs import UserRunningAndSavedLabsParameters
from lods_lib.client import ILodsApiClient
from lods_lib.datamodel.lab_instance import LabInstance
from lods_lib.datamodel.running_lab import RunningLab
from lods_lib.datamodel.saved_lab import SavedLab

from commons.dependency_injection import inject
from lods import models
from lods.exceptions import UserRunningAndSavedLabsError

import logging

logger = logging.getLogger(__name__)


class SavedRunningLabFactoryBase:
    def get_data(self, old_obj):
        raise NotImplementedError()

    def __init__(self, user):
        self.user = user

    def multi_update(self, old_objs: List[Any]) -> List[models.Lab]:
        objects = []
        for old_obj in old_objs:
            try:
                course = models.Course.objects.get(pk=old_obj.LabProfileId)
            except models.Course.DoesNotExist as e:
                inp = {"LabProfileId": old_obj.LabProfileId}
                logger.error(f"Error while updating labs. The LabProfileId doesn't refer to an existing course."
                             f"Using default=None. Input: {inp}. Error: {e}")
                course = None
            data = self.get_data(old_obj)
            obj = models.Lab(course=course, user=self.user, **data)
            objects.append(obj)
        return objects


class RunningLabFactory(SavedRunningLabFactoryBase):
    def get_data(self, old_obj: RunningLab):
        return {
            "id": old_obj.LabInstanceId,
            "start": old_obj.Start,
            "expires": old_obj.Expires,
            "url": old_obj.Url,
            "is_running": True,
            "is_saved": False,
            "is_others": False,
        }


class SavedLabFactory(SavedRunningLabFactoryBase):
    def get_data(self, old_obj: SavedLab):
        return {
            "id": old_obj.LabInstanceId,
            "minutes_remaining": old_obj.MinutesRemaining,
            "saved": old_obj.Saved,
            "expires": old_obj.Expires,
            "save_in_progress": old_obj.SaveInProgress,
            "is_running": False,
            "is_saved": True,
            "is_others": False,
        }


class LabInstanceFactory:
    def __init__(self, user):
        self.user = user

    def multi_update(self, old_objs: List[Any]) -> List[models.Lab]:
        objects = []
        for old_obj in old_objs:
            data = self.get_data(old_obj)
            obj = models.Lab(**data)
            objects.append(obj)
        return objects

    def get_data(self, old_obj: LabInstance):
        try:
            course = models.Course.objects.get(pk=old_obj.LabProfileId)
        except models.Course.DoesNotExist as e:
            inp = {"LabProfileId": old_obj.LabProfileId}
            logger.error(f"Error while updating labs. The LabProfileId doesn't refer to an existing course."
                         f"Using default=None. Input: {inp}. Error: {e}")
            course = None
        try:
            user = get_user_model().objects.get(pk=old_obj.UserId)
        except get_user_model().DoesNotExist as e:
            inp = {"UserId": old_obj.UserId}
            logger.error(f"Error while updating labs. The UserId doesn't refer to an existing user."
                         f"Using default=None. Input: {inp}. Error: {e}")
            user = None
        return {
            "id": old_obj.Id,
            "start": old_obj.Start,
            "expires": old_obj.Expires,
            "user": user,
            "course": course,
            "is_running": None,
            "is_saved": None,
            "is_others": self.user != user,
        }


class ILabsFactory:
    def populate_data(self, user, disable_admin=False) -> List[models.Lab]:
        raise NotImplementedError()


class LabsFactory:
    @inject
    def __init__(self, lods_api_client: ILodsApiClient):
        self.lods_api_client = lods_api_client

    def _populate_data_admin(self, user) -> List[models.Lab]:
        logger.info("Getting RunningAndSavedLabs from LODS")
        response = self.lods_api_client.running_and_saved_labs()
        if response.Status == 1:
            labs = LabInstanceFactory(user).multi_update(response.SavedLabs)
            labs.extend(LabInstanceFactory(user).multi_update(response.RunningLabs))
            return labs
        logger.error("Failed getting RunningAndSavedLabs.",
                     extra = {'output': response.__dict__})
        raise UserRunningAndSavedLabsError()

    def _populate_data_user(self, user) -> List[models.Lab]:
        logger.info("Getting UserRunningAndSavedLabs from LODS")
        params = UserRunningAndSavedLabsParameters(userId=user.id)
        response = self.lods_api_client.user_running_and_saved_labs(params)
        if response.Status == 1:
            labs = SavedLabFactory(user).multi_update(response.SavedLabs)
            labs.extend(RunningLabFactory(user).multi_update(response.RunningLabs))
            return labs
        logger.error("Failed getting UserRunningAndSavedLabs.",
                     extra = {'input': params.__dict__, 'output': response.__dict__})
        raise UserRunningAndSavedLabsError()

    def populate_data(self, user, disable_admin=False) -> List[models.Lab]:
        if user.is_staff and not disable_admin:
            labs = self._populate_data_user(user)
            for admin_lab in self._populate_data_admin(user):
                if admin_lab.id not in [lab.id for lab in labs]:
                    labs.append(admin_lab)
            return labs
        else:
            return self._populate_data_user(user)
