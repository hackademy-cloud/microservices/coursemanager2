from typing import Dict, Any

from lods_lib.api_methods.catalog import CatalogResponse
from lods_lib.client import ILodsApiClient
from lods_lib.datamodel.lab_profile import LabProfile
from lods_lib.datamodel.lab_series import LabSeries

from commons.dependency_injection import inject
from commons.factories import ExternalAPIAbstractFactory
from lods import models

import logging

logger = logging.getLogger(__name__)


class CourseFactory(ExternalAPIAbstractFactory):
    def __init__(self, catalog: models.Catalog):
        super().__init__(models.Course)
        self.catalog = catalog

    def get_filter_kriteria(self, old_obj: LabProfile):
        return {"id": old_obj.Id}

    def get_data(self, old_obj: LabProfile):
        return {
            "deleted": False,
            "catalog": self.catalog,
            "name": old_obj.Name,
            "number": old_obj.Number,
            "platform_id": old_obj.PlatformId,
            "cloud_platform_id": old_obj.CloudPlatformId,
            "enabled": old_obj.Enabled,
            "reason_disabled": old_obj.ReasonDisabled,
            "development_status_id": old_obj.DevelopmentStatusId,
            "description": old_obj.Description,
            "series_id": old_obj.SeriesId,
            "objective": old_obj.Objective,
            "scenario": old_obj.Scenario,
            "expected_duration_minutes": old_obj.ExpectedDurationMinutes,
            "duration_minutes": old_obj.DurationMinutes,
            "ram": old_obj.RAM,
            "has_integrated_content": old_obj.HasIntegratedContent,
            "content_version": old_obj.ContentVersion,
            "is_exam": old_obj.IsExam,
            "premium_price": old_obj.PremiumPrice,
            "basic_price": old_obj.BasicPrice,
            "tags": old_obj.Tags,
            "shared_class_environment_role_id": old_obj.SharedClassEnvironmentRoleId,
        }


class SeriesFactory(ExternalAPIAbstractFactory):
    def __init__(self, catalog: models.Catalog):
        super().__init__(models.Series)
        self.catalog = catalog

    def get_filter_kriteria(self, old_obj: LabProfile):
        return {"id": old_obj.Id}

    def get_data(self, old_obj: LabSeries) -> Dict[str, Any]:
        return {
            "deleted": False,
            "catalog": self.catalog,
            "name": old_obj.Name,
            "description": old_obj.Description,
            "num_training_days": old_obj.NumTrainingDays
        }


class CatalogFactory:
    @inject
    def __init__(self, lods_api_client: ILodsApiClient, catalog_id=1):
        self.lods_api_client = lods_api_client
        self.catalog_id = catalog_id

    def populate_data(self) -> models.Catalog:
        logger.info("Getting Catalog from LODS")
        response: CatalogResponse = self.lods_api_client.catalog()
        data = {
            "error": response.Error,
            "status": response.Status,
        }
        catalog, created = models.Catalog.objects.update_or_create(pk=self.catalog_id, defaults=data)
        if catalog.status == 1:
            SeriesFactory(catalog).multi_update(response.LabSeries)
            CourseFactory(catalog).multi_update(response.LabProfiles)
        logger.error(
            "Failed getting Catalog.",
            extra={'output': response.__dict__})
        return catalog