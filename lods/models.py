from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q

from commons.models import DeleteModel
from company.models import Company


class Catalog(models.Model):
    error = models.TextField(null=True)
    status = models.IntegerField(null=True)  # 1 = OK

    def __str__(self):
        return f"Catalog {self.id}"


class Series(DeleteModel):
    catalog = models.ForeignKey(Catalog, on_delete=models.CASCADE, null=False, related_name="series")
    name = models.TextField(null=True)
    description = models.TextField(null=True)
    num_training_days = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.name} ({self.pk})"

    def is_payed(self, user):
        # all payed series objects that are related to this course and my user.
        # if this list is empty, the course is not payed.
        my_membered_companies = Company.objects.filter(members__user=user)
        payed_series = self.payed_series.filter(Q(company__owner=user) | Q(company__in=my_membered_companies))
        return payed_series.exists()


class Course(DeleteModel):
    catalog = models.ForeignKey(Catalog, on_delete=models.CASCADE, null=False, related_name="course")
    name = models.TextField(null=True)
    number = models.TextField(null=True)
    platform_id = models.IntegerField(null=True)
    cloud_platform_id = models.IntegerField(null=True)
    enabled = models.BooleanField(null=True)
    reason_disabled = models.TextField(null=True)
    development_status_id = models.IntegerField(null=True)
    description = models.TextField(null=True)
    series = models.ForeignKey(Series, on_delete=models.DO_NOTHING, null=True, related_name="courses")
    objective = models.TextField(null=True)
    scenario = models.TextField(null=True)
    expected_duration_minutes = models.IntegerField(null=True)
    duration_minutes = models.IntegerField(null=True)
    ram = models.IntegerField(null=True)
    has_integrated_content = models.BooleanField(null=True)
    content_version = models.IntegerField(null=True)
    is_exam = models.BooleanField(null=True)
    premium_price = models.FloatField(null=True)
    basic_price = models.FloatField(null=True)
    tags = models.JSONField(null=True, default=list)
    shared_class_environment_role_id = models.IntegerField(null=True)

    def __str__(self):
        return f"{self.name} ({self.pk})"

    def is_payed(self, user):
        # all payed series objects that are related to this course and my user.
        # if this list is empty, the course is not payed.
        my_membered_companies = Company.objects.filter(members__user=user)
        payed_series = self.series.payed_series.filter(Q(company__owner=user) | Q(company__in=my_membered_companies))
        return payed_series.exists()


class DeliveryRegions(DeleteModel):
    catalog = models.ForeignKey(Catalog, on_delete=models.CASCADE, null=False, related_name="delivery_regions")
    name = models.TextField(null=True)
    description = models.TextField(null=True)

    def __str__(self):
        return f"{self.name} ({self.pk})"


class Lab(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, null=True)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING, null=True)
    minutes_remaining = models.IntegerField(null=True)
    saved = models.IntegerField(null=True)
    start = models.IntegerField(null=True)
    expires = models.IntegerField(null=True)
    save_in_progress = models.BooleanField(null=True)
    url = models.TextField(null=True)
    is_running = models.BooleanField(null=True)
    is_saved = models.BooleanField(null=True)
    is_others = models.BooleanField(null=True)

    class Meta:
        managed = False


class LabHistory(models.Model):
    user = models.ForeignKey(get_user_model(), on_delete=models.DO_NOTHING, null=True)
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING, null=True)
    action = models.TextField(null=False)
    datetime = models.DateTimeField(null=False, auto_now_add=True)
    status = models.IntegerField(null=True)
    error_message = models.TextField(null=True)

    def __str__(self):
        return f"LabHistory {self.user.email}: {self.action} {self.course.name} ({self.datetime}) ({self.status})"
