from rest_framework.exceptions import APIException


class UserRunningAndSavedLabsError(APIException):
    status_code = 500
    default_detail = 'An internal error occurred while updating the lab, please contact an administrator.'
    default_code = 'internal_server_error'


class InternalError(APIException):
    status_code = 500
    default_detail = 'An internal error occurred.'
    default_code = 'internal_server_error'


class Conflict(APIException):
    status_code = 409
    default_code = 'conflict'
