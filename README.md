# Hackademy API

Dieses Projekt soll die REST-API hinter Hackademy werden.

## Roadmap Prototype Phase:
- [x] Login und Authentication
- [x] Custom User mit E-Mail statt Username
- [x] Companies App
- [x] Learn on Demand Systems Integration

## Roadmap Beta Phase:
- [ ] Companies can add Members
- [ ] Companies can add Members with a List of Mail-Addresses
- [ ] Companies can split their Members into Departments
- [ ] User Statistics
- [ ] Companies can see Member Statistics
- [ ] Coming soon

## Running:
Die API besteht aus zwei Teilen. Einmal die Django Application, welche skalierbar ist und die REST-API enthält.
Zweitens nutzen wir die django-crontab Library für wiederkehrende Tasks in der Anwendung. Beispielsweise werden
die Daten welche von Learn on Demand Systems genutzt werden über einen Crontab im Hintergrund in einer Art warm
Caching in unsere Datenbank eingefügt. Diesen Cron-Teil sollte man wenn möglich nicht skalieren sondern nur einmal
deployen.

## Deployment der Django Application

### Docker
Es gibt in der [Gitlab Container Registry](https://gitlab.com/hackademy-cloud/microservices/coursemanager2/container_registry) zwei Docker Images:
- coursemanager-cron
- coursemanager-web

coursemanager-web kann skaliert werden und mehrfach deployed. Es enthält die Django Anwendung mit der API.

coursemanager-cron hingegen sollte nicht skaliert werden. In diesem Container werden die Cronjobs ausgeführt, welche die Datenbank mit externen Daten füllen.

You also need to run a container for postgres.

Take a look at ./docker/docker-compose-live-example.yml for an example.

### Manuelles Deployment
#### Deployment der Django Application
Take a look at the dockerfile or google how to run a django application.

#### Deployment von django-crontab
Auf dem Server wo die Cronjobs ausgeführt werden sollen, können mit diesem Befehl die Cronjobs hinzugefügt werden:
- `python manage.py crontab add`

Um die aktuell aktiven Jobs zu sehen:
- `python manage.py crontab show`

Um alle Jobs zu stoppen:
- `python manage.py crontab remove`

When you have changed something, you need to remove and add the jobs.

Mehr dazu hier:
https://pypi.org/project/django-crontab/
