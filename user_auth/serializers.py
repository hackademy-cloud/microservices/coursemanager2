from dj_rest_auth import serializers
from dj_rest_auth.registration.serializers import RegisterSerializer
from rest_framework import serializers as r_serializers


# used for rest-auth plugin
class CustomLoginSerializer(serializers.LoginSerializer):
    username = None


# used for rest-auth plugin
class CustomRegisterSerializer(RegisterSerializer):
    username = None
    first_name = r_serializers.CharField(allow_blank=False, max_length=150, trim_whitespace=True)
    last_name = r_serializers.CharField(allow_blank=True, max_length=150, trim_whitespace=True)

    def get_cleaned_data(self):
        return {
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', '')
        }
