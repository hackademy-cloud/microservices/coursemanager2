"""coursemanager2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'companies': reverse('company:company-list', request=request, format=format),
        'members': reverse('company:member-list', request=request, format=format),
        'users': reverse('user:user-list', request=request, format=format),
        'series': reverse('lods:series-list', request=request, format=format),
        'payed_series': reverse('payed_series:payed_series-list', request=request, format=format),
        'courses': reverse('lods:course-list', request=request, format=format),
        'labs': reverse('lods:lab-list', request=request, format=format),
        'lab_history': reverse('lods:lab_history-list', request=request, format=format),
    })


@api_view(['GET'])
def root(request, format=None):
    return Response({
        'api': reverse('api_root', request=request, format=format),
        'rest-auth': reverse('rest_auth_root', request=request, format=format),
    })


urlpatterns = [
    path('', root, name='root'),
    path('api/', api_root, name='api_root'),
    path('api/', include('company.urls')),
    path('api/', include('user.urls')),
    path('api/', include('lods.urls')),
    path('api/', include('payed_series.urls')),
]

# BEGIN SONSTIGE URLS
urlpatterns += [
    path('admin/', admin.site.urls),                    # contains the admin web-ui
    path('api-auth/', include('rest_framework.urls')),  # contains login and logout for the api web-ui
    path('rest-auth/', include('user_auth.urls')),      # contains login/logout/registration/profile as rest api
    # path('accounts/', include('allauth.urls')),       # contains many different authentication and user views
]
# END SONSTIGE URLS
