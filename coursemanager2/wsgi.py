"""
WSGI config for coursemanager2 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
import dotenv
from django.core.wsgi import get_wsgi_application

env_file = os.path.join(os.path.dirname(__file__), '.env')
if os.path.isfile(env_file):
    print("Loading .env file.")
    dotenv.read_dotenv(env_file)
else:
    print("No .env file found.")

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'coursemanager2.settings')

application = get_wsgi_application()
