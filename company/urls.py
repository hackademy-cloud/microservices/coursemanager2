from rest_framework.routers import DefaultRouter

from .views import CompanyViewSet, MembershipViewSet

app_name = 'company'

router = DefaultRouter()
router.register('companies', CompanyViewSet, basename='company')
router.register('members', MembershipViewSet, basename='member')

urlpatterns = router.urls
