import json

from django.contrib.auth import get_user_model
from django.urls import reverse

from commons.utils_testing import TokenLoginAPITestCase, ModelAPITestCase
from company.models import Company, Membership


class CompanyListAPIViewTestCase(TokenLoginAPITestCase):
    url = reverse("company:company-list")

    def test_get_my_company_with_permission(self):
        """You should only see your company.

        TODO: and all you are assigned to"""
        Company.objects.create(owner=self.user, name="Hackademy")
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content)) == Company.objects.count())

    def test_get_nothing_without_login(self):
        """You should see no company."""
        self.client.logout()
        Company.objects.create(owner=self.user, name="Hackademy")
        response = self.client.get(self.url)
        self.assertEqual(401, response.status_code)

    def test_get_nothing_from_others(self):
        """You should see no company from others."""
        Company.objects.create(owner=self.user, name="Hackademy")
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content)) == 1)
        self.client.logout()
        self.login_as_default_user_2()
        response = self.client.get(self.url)
        self.assertEqual(200, response.status_code)
        self.assertTrue(len(json.loads(response.content)) == 0)

    def test_get_all_companies_with_admin(self):
        """Admins should see all companies."""
        Company.objects.create(owner=self.user, name="Hackademy")
        response = self.client.get(self.url)
        self.assertTrue(len(json.loads(response.content)) == 1)
        self.client.logout()
        self.login_as_admin()
        Company.objects.create(owner=self.user, name="Hackademy2")
        response = self.client.get(self.url)
        self.assertEqual(len(json.loads(response.content)), Company.objects.count())

    def test_post(self):
        response = self.client.post(self.url, {"name": "Hackademy"})
        self.assertEqual(201, response.status_code)

    def test_post_more_companies(self):
        response = self.client.post(self.url, {"name": "Hackademy"})
        self.assertEqual(201, response.status_code)
        response = self.client.post(self.url, {"name": "Hackademy2"})
        self.assertEqual(201, response.status_code)
        response = self.client.post(self.url, {"name": "Hackademy3"})
        self.assertEqual(201, response.status_code)
        response = self.client.get(self.url)
        self.assertEqual(len(json.loads(response.content)), 3)

    def test_post_same_name(self):
        response = self.client.post(self.url, {"name": "Hackademy"})
        self.assertEqual(201, response.status_code)
        response = self.client.post(self.url, {"name": "Hackademy"})
        self.assertEqual(400, response.status_code)


class CompanyDetailBaseAPIViewTestCase(TokenLoginAPITestCase, ModelAPITestCase):
    """Creates a default company in setUp."""

    model = Company

    def url(self, pk):
        return reverse("company:company-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.obj = Company.objects.create(owner=self.user, name="Hackademy")
        self.obj_json = {"id": self.obj.id, "name": "Hackademy", "owner": {"email": "user@domain.com", "id": self.user.id}, "members": []}


class CompanyGetDetailAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):
    def test_get_my_company_with_permission(self):
        """You should only see your company."""
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(response.data, self.obj_json)

    def test_get_nothing_from_others(self):
        """You should see no company from others."""
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(response.data, self.obj_json)
        self.client.logout()
        self.login_as_default_user_2()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(404, response.status_code)

    def test_get_all_companies_with_admin(self):
        """Admins should see all companies."""
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(response.data, self.obj_json)
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(response.data, self.obj_json)

    def test_get_nothing_without_login(self):
        """You should see no company."""
        self.client.logout()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(401, response.status_code)


class CompanyDeleteDetailAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):

    def test_delete_my_company_with_permission(self):
        """You should only delete your company."""
        response = self.client.delete(self.url(self.obj.id))
        self.assertDeleted(response, self.obj.id)

    def test_delete_nothing_from_others(self):
        """You should delete no company from others."""
        self.client.logout()
        self.login_as_default_user_2()
        response = self.client.delete(self.url(self.obj.id))
        self.assertEqual(404, response.status_code)

    def test_delete_all_companies_with_admin(self):
        """Admins should delete all companies."""
        self.client.logout()
        self.login_as_admin()
        response = self.client.delete(self.url(self.obj.id))
        self.assertDeleted(response, self.obj.id)

    def test_delete_nothing_without_login(self):
        """You should see no company."""
        self.client.logout()
        response = self.client.delete(self.url(self.obj.id))
        self.assertEqual(401, response.status_code)


class CompanyPutDetailAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):
    """Update full object"""

    def test_put_my_company_with_permission_cant_change_id(self):
        response = self.client.put(self.url(self.obj.id), {"id": self.obj.id + 1, "name": "Hackademy"})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_put_my_company_with_permission_cant_change_owner(self):
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy", "owner": {"email": "user2@domain.com", "id": self.user.id + 1}}, format="json")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_put_my_company_with_permission_cant_change_name_duplicate(self):
        Company.objects.create(owner=self.user, name="Hackademy2")
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(400, response.status_code)

    def test_put_my_company_with_permission_change_name(self):
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(200, response.status_code)
        new_obj_json = self.obj_json
        new_obj_json["name"] = "Hackademy2"
        self.assertEqual(response.data, new_obj_json)

    def test_put_my_company_with_admin_cant_change_id(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.put(self.url(self.obj.id), {"id": self.obj.id + 1, "name": "Hackademy"})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_put_my_company_with_admin_cant_change_name_duplicate(self):
        self.client.logout()
        self.login_as_admin()
        Company.objects.create(owner=self.user, name="Hackademy2")
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(400, response.status_code)

    def test_put_my_company_with_admin_change_name(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(200, response.status_code)
        new_obj_json = self.obj_json
        new_obj_json["name"] = "Hackademy2"
        self.assertEqual(response.data, new_obj_json)

    def test_put_my_company_with_admin_change_owner(self):
        new_user = get_user_model().objects.create_user("another@user.de", "geheim")
        self.client.logout()
        self.login_as_admin()
        new_obj_json_owner = {"email": new_user.email, "id": new_user.id}
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy", "owner_id": new_user.id}, format="json")
        new_obj_json = self.obj_json
        new_obj_json["owner"] = new_obj_json_owner
        self.assertEqual(response.data, new_obj_json)

    def test_put_nothing_from_others_change_name(self):
        self.client.logout()
        self.login_as_default_user_2()
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(404, response.status_code)

    def test_put_nothing_without_login_change_name(self):
        self.client.logout()
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(401, response.status_code)


class CompanyPatchDetailAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):
    """Partial update"""

    def test_patch_my_company_with_permission_cant_change_id(self):
        response = self.client.patch(self.url(self.obj.id), {"id": self.obj.id + 1})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_patch_my_company_with_permission_cant_change_owner(self):
        response = self.client.patch(self.url(self.obj.id), {"owner": {"email": "user2@domain.com", "id": self.user.id + 1}}, format="json")
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_patch_my_company_with_permission_cant_change_name_duplicate(self):
        Company.objects.create(owner=self.user, name="Hackademy2")
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(400, response.status_code)

    def test_patch_my_company_with_permission_change_name(self):
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(200, response.status_code)
        new_obj_json = self.obj_json
        new_obj_json["name"] = "Hackademy2"
        self.assertEqual(response.data, new_obj_json)

    def test_patch_my_company_with_admin_cant_change_id(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.patch(self.url(self.obj.id), {"id": self.obj.id + 1})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.data, self.obj_json)

    def test_patch_my_company_with_admin_cant_change_name_duplicate(self):
        self.client.logout()
        self.login_as_admin()
        Company.objects.create(owner=self.user, name="Hackademy2")
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(400, response.status_code)

    def test_patch_my_company_with_admin_change_name(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(200, response.status_code)
        new_obj_json = self.obj_json
        new_obj_json["name"] = "Hackademy2"
        self.assertEqual(response.data, new_obj_json)

    def test_patch_my_company_with_admin_change_owner(self):
        new_user = get_user_model().objects.create_user("another@user.de", "geheim")
        self.client.logout()
        self.login_as_admin()
        new_obj_json_owner = {"email": new_user.email, "id": new_user.id}
        response = self.client.patch(self.url(self.obj.id), {"owner_id": new_user.id}, format="json")
        new_obj_json = self.obj_json
        new_obj_json["owner"] = new_obj_json_owner
        self.assertEqual(response.data, new_obj_json)

    def test_patch_nothing_from_others_change_name(self):
        self.client.logout()
        self.login_as_default_user_2()
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(404, response.status_code)

    def test_patch_nothing_without_login_change_name(self):
        self.client.logout()
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(401, response.status_code)


class CompanyOwnerAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):
    """Tests the owner relation."""

    def test_delete_owner(self):
        """If the owner is deleted, the company should be deleted too."""
        user_id = self.user.id
        self.user.delete()
        self.assertFalse(get_user_model().objects.filter(pk=user_id).exists())
        self.login_as_admin()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(404, response.status_code)


class CompanyMemberAPIViewTestCase(CompanyDetailBaseAPIViewTestCase):
    """Tests the member relation."""

    def test_member_can_get_company_list(self):
        """Members should see there company."""
        self.login_as_default_user_2()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(404, response.status_code)
        member = Membership.objects.create(user=self.user, company=self.obj)
        self.obj_json["members"] = [{"id": member.id, "user_id": self.user.id, "email": self.user.email}]
        response = self.client.get(reverse("company:company-list"))
        self.assertListEqual(response.data, [self.obj_json])

    def test_member_can_get_company_detail(self):
        """Members should see there company."""
        self.login_as_default_user_2()
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(404, response.status_code)
        member = Membership.objects.create(user=self.user, company=self.obj)
        self.obj_json["members"] = [{"id": member.id, "user_id": self.user.id, "email": self.user.email}]
        response = self.client.get(self.url(self.obj.id))
        self.assertEqual(response.data, self.obj_json)

    def test_member_cant_patch_company(self):
        """Members shouldn't patch there company."""
        self.login_as_default_user_2()
        Membership.objects.create(user=self.user, company=self.obj)
        response = self.client.patch(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(403, response.status_code)

    def test_member_cant_put_company(self):
        """Members shouldn't patch there company."""
        self.login_as_default_user_2()
        Membership.objects.create(user=self.user, company=self.obj)
        response = self.client.put(self.url(self.obj.id), {"name": "Hackademy2"})
        self.assertEqual(403, response.status_code)

    def test_member_cant_delete_company(self):
        """Members shouldn't patch there company."""
        self.login_as_default_user_2()
        Membership.objects.create(user=self.user, company=self.obj)
        response = self.client.delete(self.url(self.obj.id))
        self.assertEqual(403, response.status_code)


class MembershipAPIViewTestCase(TokenLoginAPITestCase, ModelAPITestCase):

    model = Membership
    list_url = reverse("company:member-list")

    def detail_url(self, pk):
        return reverse("company:member-detail", args=[pk])

    def login_as_member(self):
        self.login(self.member)

    def setUp(self):
        super().setUp()
        self.company = Company.objects.create(owner=self.user, name="Hackademy")
        self.member = get_user_model().objects.create_user("member@member.de", "geheim")
        self.membership = Membership.objects.create(user=self.member, company=self.company)
        self.user_json = {"id": self.member.id, "email": self.member.email}
        self.company_json = {"id": self.company.id, "name": self.company.name}
        self.membership_json = {"id": self.membership.id, "user": self.user_json, "company": self.company_json}

    def test_get_with_owner(self):
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count())

    def test_get_with_owner_detail(self):
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(response.data, self.membership_json)

    def test_get_with_owner_from_other_company(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        Membership.objects.create(user=member2, company=company2)
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count() - 1)

    def test_get_with_owner_from_other_company_detail(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        membership2 = Membership.objects.create(user=member2, company=company2)
        response = self.client.get(self.detail_url(membership2.id), format="json")
        self.assertEqual(404, response.status_code)

    def test_get_with_admin(self):
        self.login_as_admin()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count())

    def test_get_with_admin_detail(self):
        self.login_as_admin()
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(response.data, self.membership_json)

    def test_get_with_admin_from_other_company(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        Membership.objects.create(user=member2, company=company2)
        self.login_as_admin()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count())

    def test_get_with_admin_from_other_company_detail(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        Membership.objects.create(user=member2, company=company2)
        self.login_as_admin()
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(response.data, self.membership_json)

    def test_get_with_member(self):
        self.login_as_member()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count())

    def test_get_with_member_detail(self):
        self.login_as_member()
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(response.data, self.membership_json)

    def test_get_with_member_from_other_company(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        Membership.objects.create(user=member2, company=company2)
        self.login_as_member()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), Membership.objects.count() - 1)

    def test_get_with_member_from_other_company_detail(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        membership2 = Membership.objects.create(user=member2, company=company2)
        self.login_as_member()
        response = self.client.get(self.detail_url(membership2.id), format="json")
        self.assertEqual(404, response.status_code)

    def test_get_nothing_with_authenticated(self):
        self.login_as_default_user_2()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_get_nothing_with_authenticated_detail(self):
        self.login_as_default_user_2()
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(404, response.status_code)

    def test_get_nothing_with_non_authenticated(self):
        self.client.logout()
        response = self.client.get(self.list_url)
        self.assertEqual(401, response.status_code)

    def test_get_nothing_with_non_authenticated_detail(self):
        self.client.logout()
        response = self.client.get(self.detail_url(self.membership.id), format="json")
        self.assertEqual(401, response.status_code)

    def test_post_admin(self):
        company2 = Company.objects.create(owner=self.user, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        self.login_as_admin()
        response = self.client.post(self.list_url, {"user_id": member2.id, "company_id": company2.id})
        self.assertEqual(201, response.status_code)

    def test_post_owner(self):
        company2 = Company.objects.create(owner=self.user, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        response = self.client.post(self.list_url, {"user_id": member2.id, "company_id": company2.id})
        self.assertEqual(403, response.status_code)

    def test_post_authenticated(self):
        company2 = Company.objects.create(owner=self.user, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        self.login_as_default_user_2()
        response = self.client.post(self.list_url, {"user_id": member2.id, "company_id": company2.id})
        self.assertEqual(403, response.status_code)

    def test_post_non_authenticated(self):
        company2 = Company.objects.create(owner=self.user, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        self.client.logout()
        response = self.client.post(self.list_url, {"user_id": member2.id, "company_id": company2.id})
        self.assertEqual(401, response.status_code)

    def test_put_not_working(self):
        put = {"user_id": self.user.id, "company_id": self.company.id}
        # as owner
        response = self.client.put(self.detail_url(self.membership.id), put)
        self.assertEqual(403, response.status_code)
        self.login_as_admin()
        response = self.client.put(self.detail_url(self.membership.id), put)
        self.assertEqual(403, response.status_code)
        self.login_as_member()
        response = self.client.put(self.detail_url(self.membership.id), put)
        self.assertEqual(403, response.status_code)
        self.login_as_default_user_2()
        response = self.client.put(self.detail_url(self.membership.id), put)
        self.assertEqual(404, response.status_code)
        self.client.logout()
        response = self.client.put(self.detail_url(self.membership.id), put)
        self.assertEqual(401, response.status_code)

    def test_patch_not_working(self):
        patch = {"user_id": self.user.id, "company_id": self.company.id}
        # as owner
        response = self.client.patch(self.detail_url(self.membership.id), patch)
        self.assertEqual(403, response.status_code)
        self.login_as_admin()
        response = self.client.patch(self.detail_url(self.membership.id), patch)
        self.assertEqual(403, response.status_code)
        self.login_as_member()
        response = self.client.patch(self.detail_url(self.membership.id), patch)
        self.assertEqual(403, response.status_code)
        self.login_as_default_user_2()
        response = self.client.patch(self.detail_url(self.membership.id), patch)
        self.assertEqual(404, response.status_code)
        self.client.logout()
        response = self.client.patch(self.detail_url(self.membership.id), patch)
        self.assertEqual(401, response.status_code)

    def test_delete_with_owner(self):
        response = self.client.delete(self.detail_url(self.membership.id))
        self.assertEqual(204, response.status_code)

    def test_delete_with_owner_from_other_company(self):
        owner2 = get_user_model().objects.create_user("member3@member.de", "geheim")
        company2 = Company.objects.create(owner=owner2, name="Hackademy2")
        member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        membership2 = Membership.objects.create(user=member2, company=company2)
        response = self.client.delete(self.detail_url(membership2.id))
        self.assertEqual(404, response.status_code)

    def test_delete_with_admin(self):
        self.login_as_admin()
        response = self.client.delete(self.detail_url(self.membership.id))
        self.assertEqual(204, response.status_code)

    def test_delete_with_member(self):
        self.login_as_member()
        response = self.client.delete(self.detail_url(self.membership.id))
        self.assertEqual(403, response.status_code)

    def test_delete_with_authorized(self):
        self.login_as_default_user_2()
        response = self.client.delete(self.detail_url(self.membership.id))
        self.assertEqual(404, response.status_code)

    def test_delete_with_non_authorized(self):
        self.client.logout()
        response = self.client.delete(self.detail_url(self.membership.id))
        self.assertEqual(401, response.status_code)


class MembershipFilterAPIViewTestCase(TokenLoginAPITestCase, ModelAPITestCase):

    model = Membership
    list_url = reverse("company:member-list")

    def login_as_user(self, user):
        self.login(user)

    @staticmethod
    def member_json(member):
        return {"id": member.id, "email": member.email}

    @staticmethod
    def company_json(company):
        return {"id": company.id, "name": company.name}

    @staticmethod
    def membership_json(membership):
        company_json = MembershipFilterAPIViewTestCase.company_json(membership.company)
        user_json = MembershipFilterAPIViewTestCase.membership_json(membership.user)
        return {"id": membership.id, "user": user_json, "company": company_json}

    def setUp(self):
        super().setUp()
        self.user1 = get_user_model().objects.create_user("user1@member.de", "geheim")
        self.user2 = get_user_model().objects.create_user("user2@member.de", "geheim")
        self.user3 = get_user_model().objects.create_user("user3@member.de", "geheim")
        self.company1 = Company.objects.create(owner=self.user1, name="Hackademy")
        self.company2 = Company.objects.create(owner=self.user2, name="Hackademy2")
        self.company3 = Company.objects.create(owner=self.user3, name="Hackademy3")
        self.member1 = get_user_model().objects.create_user("member1@member.de", "geheim")
        self.member2 = get_user_model().objects.create_user("member2@member.de", "geheim")
        self.member3 = get_user_model().objects.create_user("member3@member.de", "geheim")
        self.membership1 = Membership.objects.create(user=self.member1, company=self.company1)
        self.membership2 = Membership.objects.create(user=self.member1, company=self.company2)
        self.membership3 = Membership.objects.create(user=self.member1, company=self.company3)
        self.membership4 = Membership.objects.create(user=self.member2, company=self.company1)
        self.membership5 = Membership.objects.create(user=self.member2, company=self.company2)
        self.membership6 = Membership.objects.create(user=self.member3, company=self.company1)

    def test_get_with_admin(self):
        self.login_as_admin()
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), 6)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 2)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 2)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)

    def test_get_with_owner(self):
        self.login_as_user(self.user1)
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        self.login_as_user(self.user2)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 2)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        self.login_as_user(self.user3)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)

    def test_get_with_member(self):
        self.login_as_user(self.member1)
        response = self.client.get(self.list_url)
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 3)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        self.login_as_user(self.member2)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 2)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        self.login_as_user(self.member3)
        response = self.client.get(f"{self.list_url}?company_id={self.company1.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
        response = self.client.get(f"{self.list_url}?company_id={self.company2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?company_id={self.company3.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member1.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member2.id}")
        self.assertEqual(len(json.loads(response.content)), 0)
        response = self.client.get(f"{self.list_url}?user_id={self.member3.id}")
        self.assertEqual(len(json.loads(response.content)), 1)
