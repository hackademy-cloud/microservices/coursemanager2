from django.contrib.auth import get_user_model
from rest_framework import serializers

from company.models import Company, Membership


class CompanyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email']


class CompanyMembershipSerializer(serializers.ModelSerializer):
    user_id = serializers.IntegerField(source="user.id")
    email = serializers.CharField(source="user.email")

    class Meta:
        model = Membership
        fields = ['id', 'user_id', 'email']


class CompanySerializerAdmin(serializers.ModelSerializer):
    owner = CompanyUserSerializer(many=False, read_only=True)
    owner_id = serializers.IntegerField(required=False, write_only=True)
    members = CompanyMembershipSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'owner', 'owner_id', 'members']


class CompanySerializer(serializers.ModelSerializer):
    owner = CompanyUserSerializer(many=False, read_only=True)
    members = CompanyMembershipSerializer(many=True, read_only=True)

    class Meta:
        model = Company
        fields = ['id', 'name', 'owner', 'members']
        depth = 1


class MembershipCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name']


class MembershipSerializer(serializers.ModelSerializer):
    user = CompanyUserSerializer(many=False, read_only=True)
    user_id = serializers.IntegerField(required=False, write_only=True)
    company = MembershipCompanySerializer(many=False, read_only=True)
    company_id = serializers.IntegerField(required=False, write_only=True)

    class Meta:
        model = Membership
        fields = ['id', 'user', 'user_id', 'company', 'company_id']
