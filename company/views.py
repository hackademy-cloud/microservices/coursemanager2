import rules
from django.db.models import Q
from rest_framework import permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from commons.permissions import RuleBasedPermission, is_true, is_admin, is_false
from company.models import Company, Membership
from company.serializers import CompanySerializer, CompanySerializerAdmin, MembershipSerializer, \
    CompanyMembershipSerializer


@rules.predicate
def is_owner(request, obj):
    return bool(request.user == obj.owner)


@rules.predicate
def is_member(request, obj):
    return bool(request.user in [member.user for member in obj.members.all()])


@rules.predicate
def is_owner_from_related_company(request, obj):
    return bool(request.user == obj.company.owner)


@rules.predicate
def is_member_from_related_company(request, obj):
    return bool(request.user == obj.user)


class CompanyPermission(RuleBasedPermission):
    message = "You don't have the correct permissions on this company."
    safe_rules = [is_member, is_owner, is_admin]
    post_rules = [is_true]
    change_rules = [is_owner, is_admin]
    delete_rules = [is_owner, is_admin]


class MembershipPermission(RuleBasedPermission):
    obj_name = "membership"
    message = "You don't have the correct permissions on this membership."
    safe_rules = [is_owner_from_related_company, is_admin, is_member_from_related_company]
    post_rules = [is_admin]
    change_rules = [is_false]
    delete_rules = [is_owner_from_related_company, is_admin]


class CompanyViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, CompanyPermission]
    queryset = Company.objects.all()

    def get_serializer_class(self):
        if self.action == 'members':
            return CompanyMembershipSerializer
        if is_admin(self.request):
            return CompanySerializerAdmin
        else:
            return CompanySerializer

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if is_admin(self.request):
            return queryset
        return queryset.filter(Q(owner=self.request.user) | Q(members__user=self.request.user))

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @action(detail=True)
    def members(self, request, pk=None):
        company = self.get_object()
        serializer = self.get_serializer(company.members.all(), many=True)
        return Response(serializer.data)


class MembershipViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.IsAuthenticated, MembershipPermission]
    queryset = Membership.objects.all()
    serializer_class = MembershipSerializer
    filterset_fields = ['user_id', 'company_id']

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if not is_admin(self.request):
            # all memberships that i'm allowed to see
            queryset = queryset.filter(Q(user=self.request.user) | Q(company__owner=self.request.user))
        return queryset
