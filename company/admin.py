from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db.models import Q

from company.models import Company, Membership
from payed_series.models import PayedSeries


class OwnerOrMemberListFilter(admin.SimpleListFilter):
    title = 'member or owner'
    parameter_name = 'insider'
    show_all_users = True

    def lookups(self, request, model_admin):
        if self.show_all_users:
            users = get_user_model().objects.all()
        else:
            queryset = model_admin.get_queryset(request)
            all = queryset.all()
            owners = [company.owner for company in all]
            members = [member for company in queryset.all() for member in company.members.all()]
            users = set(owners).union(members)
        return [(user.email, user.email) for user in users]

    def queryset(self, request, queryset):
        value = self.value()
        if value is None:
            return queryset
        return queryset.filter(Q(members__user__email=value) | Q(owner__email=value))


class MemberInline(admin.TabularInline):
    model = Membership
    extra = 1


class PayedSeriesInline(admin.TabularInline):
    model = PayedSeries
    extra = 1


class CompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ('name', 'owner')
    list_filter = ('owner', OwnerOrMemberListFilter,)
    inlines = (MemberInline, PayedSeriesInline)


admin.site.register(Company, CompanyAdmin)
admin.site.register(Membership)
