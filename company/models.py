from django.db import models

from coursemanager2.settings import AUTH_USER_MODEL


class Company(models.Model):
    name = models.CharField(null=False, unique=True, max_length=200)
    owner = models.ForeignKey(AUTH_USER_MODEL, null=False, on_delete=models.CASCADE, related_name="owned_companies")

    def __str__(self):
        return self.name


class Membership(models.Model):
    class Meta:
        unique_together = [['user', 'company']]

    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, null=False, related_name="companies")
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=False, related_name="members")
    date_joined = models.DateField(auto_now_add=True, blank=True, null=False)

    def __str__(self):
        return f"{self.company.name} - {self.user.email}"
