FROM python:3.8
LABEL maintainer="Marco Schlicht <m.schlicht@hackademy.cloud>"

ENV PYTHONUNBUFFERED=1

# django-cron will add this to every cronjob so that the logs of the cronjobs will be written into this files
ENV ENABLE_CRON_PIPE=True
ENV CRON_PIPE="2>&1 >> /var/log/cron.log"

# install cron
RUN apt-get update && apt-get install cron -y

# required for postgres
RUN apt-get install libpq-dev -y

# install requirements
RUN mkdir -p /opt/app/coursemanager2
WORKDIR /opt/app/coursemanager2
COPY requirements.txt /opt/app/coursemanager2
RUN pip install -r requirements.txt

# copy application with right permissions
COPY . /opt/app/coursemanager2

# create log file
RUN touch /var/log/cron.log

# start cron
CMD ["/opt/app/coursemanager2/cron-entrypoint.sh"]
