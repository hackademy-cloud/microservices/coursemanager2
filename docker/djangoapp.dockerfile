FROM python:3.8
LABEL maintainer="Marco Schlicht <m.schlicht@hackademy.cloud>"

ENV PYTHONUNBUFFERED=1

# install nginx
RUN apt-get update && apt-get install nginx vim -y --no-install-recommends
COPY nginx.default /etc/nginx/sites-available/default
RUN ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log

# required for postgres
RUN apt-get install libpq-dev -y

# install requirements
RUN mkdir -p /opt/app/coursemanager2
WORKDIR /opt/app/coursemanager2
COPY requirements.txt /opt/app/coursemanager2
RUN pip install -r requirements.txt

# copy application with right permissions
COPY . /opt/app/coursemanager2
RUN mkdir -p /opt/app/coursemanager2/static
RUN chown -R www-data:www-data /opt/app

EXPOSE 8020

CMD ["/opt/app/coursemanager2/docker-entrypoint.sh"]