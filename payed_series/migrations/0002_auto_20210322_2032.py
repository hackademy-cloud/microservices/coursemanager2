# Generated by Django 3.1.7 on 2021-03-22 20:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payed_series', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payedseries',
            name='payed',
            field=models.DateField(blank=True, default=datetime.date.today, null=True),
        ),
        migrations.AlterField(
            model_name='payedseries',
            name='payed_from',
            field=models.DateField(blank=True, default=datetime.date.today, null=True),
        ),
    ]
