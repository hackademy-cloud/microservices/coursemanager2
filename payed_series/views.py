from django.db.models import Q
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from commons.permissions import is_admin
from company.models import Company
from payed_series.models import PayedSeries
from payed_series.serializers import PayedSeriesSerializer


class PayedSeriesViewSet(viewsets.ReadOnlyModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = PayedSeries.objects.all()
    serializer_class = PayedSeriesSerializer
    filterset_fields = ['series_id', 'company_id']

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)
        if is_admin(self.request):
            return queryset
        my_membered_companies = Company.objects.filter(members__user=self.request.user)
        ret = queryset.filter(Q(company__owner=self.request.user) | Q(company__in=my_membered_companies))
        return ret
