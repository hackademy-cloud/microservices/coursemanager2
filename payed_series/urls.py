from rest_framework.routers import DefaultRouter

from .views import PayedSeriesViewSet

app_name = 'payed_series'

router = DefaultRouter()
router.register('payed_series', PayedSeriesViewSet, basename='payed_series')

urlpatterns = router.urls
