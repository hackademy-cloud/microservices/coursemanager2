from typing import Any, Dict

from django.urls import reverse

from commons.utils_testing import TokenLoginAPITestCase
from company.models import Company, Membership
from lods.models import Course, Series, Catalog
from payed_series.models import PayedSeries


def payed_series_json(payed_series: PayedSeries) -> Dict[str, Any]:
    return {
        'id': payed_series.id,
        'company': {
            'id': payed_series.company.id,
            'name': payed_series.company.name
        },
        'series': {
            'id': payed_series.series.id,
            'name': payed_series.series.name,
            'description': payed_series.series.description,
            'num_training_days': payed_series.series.num_training_days,
        },
        'payed': str(payed_series.payed),
        'payed_from': str(payed_series.payed_from),
        'payed_until': str(payed_series.payed_until),
        'valid_payment': payed_series.valid_payment
    }


class PayedSeriesRawAPIViewTestCaseBase(TokenLoginAPITestCase):
    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=1)
        self.series_1 = Series.objects.create(
            catalog=self.catalog, name="XSS"
        )
        self.series_2 = Series.objects.create(
            catalog=self.catalog, name="Buffer Overflow"
        )
        self.course_1 = Course.objects.create(
            catalog=self.catalog, name="XSS für Anfänger",
            enabled=True, series_id=self.series_1.id,
        )
        self.course_2 = Course.objects.create(
            catalog=self.catalog, name="Buffer für Anfänger",
            enabled=True, series_id=self.series_2.id,
        )
        self.owner_1 = self.user
        self.owner_2 = self.create_user("pay_user_2@example.com", "geheim")
        self.owner_3 = self.create_user("pay_user_2_2@example.com", "geheim")
        self.member_1 = self.create_user("pay_user_3@example.com", "geheim")
        self.member_2 = self.create_user("pay_user_4@example.com", "geheim")
        self.member_3 = self.create_user("pay_user_5@example.com", "geheim")
        self.company_1 = Company.objects.create(name="Google", owner=self.owner_1)
        self.company_2 = Company.objects.create(name="Facebook", owner=self.owner_2)
        self.company_3 = Company.objects.create(name="Youtube", owner=self.owner_3)
        self.membership_1 = Membership.objects.create(user=self.member_1, company=self.company_1)
        self.membership_2 = Membership.objects.create(user=self.member_2, company=self.company_1)
        self.membership_3 = Membership.objects.create(user=self.member_3, company=self.company_2)
        self.membership_4 = Membership.objects.create(user=self.owner_3, company=self.company_3)


class PayedSeriesAPIViewTestCaseBase(PayedSeriesRawAPIViewTestCaseBase):
    def setUp(self):
        super().setUp()
        self.payed_series_1 = PayedSeries.objects.create(company=self.company_1, series=self.series_1)
        self.payed_series_2 = PayedSeries.objects.create(company=self.company_2, series=self.series_2)
        self.payed_series_3 = PayedSeries.objects.create(company=self.company_3, series=self.series_2)
        self.payed_series_1_json = payed_series_json(self.payed_series_1)
        self.payed_series_2_json = payed_series_json(self.payed_series_2)
        self.payed_series_3_json = payed_series_json(self.payed_series_3)


class CourseListAPIViewTestCase(PayedSeriesAPIViewTestCaseBase):
    url = reverse("payed_series:payed_series-list")

    def test_get_payed_series_without_admin(self):
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_with_owner_2(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_with_member_1(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_with_member_and_owner(self):
        self.client.logout()
        self.login(self.owner_3)
        response = self.client.get(self.url)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 401)


class CourseDetailAPIViewTestCase(PayedSeriesAPIViewTestCaseBase):

    def url(self, pk):
        return reverse("payed_series:payed_series-detail", args=[pk])

    def test_get_payed_series_without_admin(self):
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertDictEqual(response.data, self.payed_series_1_json)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.url(self.payed_series_2.id))
        self.assertEqual(response.status_code, 404)

    def test_get_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertDictEqual(response.data, self.payed_series_1_json)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.url(self.payed_series_2.id))
        self.assertDictEqual(response.data, self.payed_series_2_json)
        self.assertEqual(response.status_code, 200)

    def test_get_payed_series_with_owner_2(self):
        self.client.logout()
        self.login(self.owner_2)
        response = self.client.get(self.url(self.payed_series_2.id))
        self.assertDictEqual(response.data, self.payed_series_2_json)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 404)

    def test_get_payed_series_with_member_1(self):
        self.client.logout()
        self.login(self.member_1)
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertDictEqual(response.data, self.payed_series_1_json)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.url(self.payed_series_2.id))
        self.assertEqual(response.status_code, 404)

    def test_get_payed_series_with_admin_and_owner(self):
        self.client.logout()
        self.login(self.owner_3)
        response = self.client.get(self.url(self.payed_series_3.id))
        self.assertDictEqual(response.data, self.payed_series_3_json)
        self.assertEqual(response.status_code, 200)
        response = self.client.get(self.url(self.payed_series_2.id))
        self.assertEqual(response.status_code, 404)
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 404)

    def test_get_payed_series_without_login(self):
        self.client.logout()
        response = self.client.get(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 401)


class LabProfilePostAPIViewTestCase(PayedSeriesRawAPIViewTestCaseBase):
    url = reverse("payed_series:payed_series-list")

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_post_payed_series_with_owner(self):
        response = self.client.post(self.url, data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(len(PayedSeries.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(PayedSeries.objects.all()), 0)

    def test_post_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.post(self.url, data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(len(PayedSeries.objects.all()), 0)
        self.assertEqual(response.status_code, 405)
        self.assertEqual(len(PayedSeries.objects.all()), 0)

    def test_post_payed_series_without_login(self):
        self.client.logout()
        response = self.client.post(self.url, data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(len(PayedSeries.objects.all()), 0)
        self.assertEqual(response.status_code, 401)
        self.assertEqual(len(PayedSeries.objects.all()), 0)


class LabCoursePutAPIViewTestCase(PayedSeriesAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("payed_series:payed_series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_put_payed_series_with_owner(self):
        response = self.client.put(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 405)

    def test_put_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.put(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 405)

    def test_put_payed_series_without_login(self):
        self.client.logout()
        response = self.client.put(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 401)


class LabCoursePatchAPIViewTestCase(PayedSeriesAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("payed_series:payed_series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_patch_payed_series_with_owner(self):
        response = self.client.patch(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 405)

    def test_patch_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.patch(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 405)

    def test_patch_payed_series_without_login(self):
        self.client.logout()
        response = self.client.patch(self.url(self.payed_series_1.id), data={
            "company_id": self.company_1.id, "series_id": self.series_1
        })
        self.assertEqual(response.status_code, 401)


class LabCourseDeleteAPIViewTestCase(PayedSeriesAPIViewTestCaseBase):
    def url(self, pk):
        return reverse("payed_series:payed_series-detail", args=[pk])

    def setUp(self):
        super().setUp()
        self.catalog = Catalog.objects.create(error="", status=0)

    def test_delete_payed_series_without_admin(self):
        response = self.client.delete(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_payed_series_with_admin(self):
        self.client.logout()
        self.login_as_admin()
        response = self.client.delete(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 405)

    def test_delete_payed_series_without_login(self):
        self.client.logout()
        response = self.client.delete(self.url(self.payed_series_1.id))
        self.assertEqual(response.status_code, 401)

