import datetime
from datetime import date

from django.db import models

from company.models import Company
from lods.models import Series


def timedelta_years():
    """Return a date that's `years` years after now
    Return the same calendar date (month and day) in the
    destination year, if it exists, otherwise use the following day
    (thus changing February 29 to March 1).

    """
    years = 1
    d = datetime.date.today()
    try:
        return d.replace(year=d.year + years)
    except ValueError:
        return d + (date(d.year + years, 1, 1) - date(d.year, 1, 1))


class PayedSeries(models.Model):
    series = models.ForeignKey(Series, on_delete=models.CASCADE, null=False, related_name="payed_series")
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=False, related_name="payed_series")
    timestamp = models.DateTimeField(null=False, blank=True, auto_now_add=True)
    payed = models.DateField(null=True, blank=True, auto_now_add=False, default=datetime.date.today)
    payed_from = models.DateField(null=True, blank=True, auto_now_add=False, default=datetime.date.today)
    payed_until = models.DateField(null=True, blank=True, auto_now_add=False, default=timedelta_years)

    @property
    def valid_payment(self) -> int:
        """Returns if the payment is valid.

        -1 = not yet valid
        0 = valid
        +1 = no longer valid
        payed_from is null, than it's not valid
        payed_until is null, than it's valid forever
        payed_from is today, it's valid
        payed_from is tomorrow, it's not valid
        payed_until is today, it's valid
        payed_until is yesterday, it's not valid
        """
        now = datetime.date.today()
        if self.payed_from is None:
            return -1
        if self.payed_from > now:
            return -1
        if self.payed_until is None or now <= self.payed_until:
            return 0
        return 1

    def __str__(self):
        return f"{self.company.name} - {self.series.name}"