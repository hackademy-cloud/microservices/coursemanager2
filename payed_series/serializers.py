from django.contrib.auth import get_user_model
from rest_framework import serializers

from company.models import Company
from lods.models import Series
from payed_series.models import PayedSeries


class PayedSeriesCompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ['id', 'name']


class PayedSeriesSeriesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Series
        fields = ["id", "name", "description", "num_training_days"]


class PayedSeriesSerializer(serializers.ModelSerializer):
    company = PayedSeriesCompanySerializer(many=False, read_only=True)
    company_id = serializers.IntegerField(required=False, write_only=True)
    series = PayedSeriesSeriesSerializer(many=False, read_only=True)
    series_id = serializers.IntegerField(required=False, write_only=True)

    class Meta:
        model = PayedSeries
        fields = ['id', 'company', 'company_id', 'series', 'series_id',
                  'payed', 'payed_from', 'payed_until', 'valid_payment']
