from django.apps import AppConfig


class PayedSeriesConfig(AppConfig):
    name = 'payed_series'
