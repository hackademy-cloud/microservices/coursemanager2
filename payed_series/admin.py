from django.contrib import admin
from payed_series.models import PayedSeries


class PayedSeriesAdmin(admin.ModelAdmin):
    pass


admin.site.register(PayedSeries, PayedSeriesAdmin)
